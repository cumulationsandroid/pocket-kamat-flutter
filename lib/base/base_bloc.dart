import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';

/// Base class for the blocs 
abstract class BaseBloc<Event, State> extends Bloc<Event, State> {}