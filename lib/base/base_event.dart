import 'package:equatable/equatable.dart';

/// Base class of the event corresponding to a particular bloc.
abstract class BaseEvent extends Equatable{}