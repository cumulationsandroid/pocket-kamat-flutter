import 'package:equatable/equatable.dart';

/// Base class of the state corresponding to a particular bloc.
abstract class BaseState extends Equatable {}
