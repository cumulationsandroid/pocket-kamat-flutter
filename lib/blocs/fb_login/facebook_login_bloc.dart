import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:pocket_kamat/common/utils.dart';
import 'package:pocket_kamat/screens/login/facebook_login_screen.dart';

import '../../common/error.dart';
import '../../common/routes.dart';
import '../../injector.dart';
import 'facebook_login_event.dart';
import 'facebook_login_state.dart';
import 'package:http/http.dart' as http;

/// This Bloc used to login an user using [AuthTokenApiService].
///
/// [LoginEvent] corresponds to the appropriate events that can be added to this
/// bloc. [LoginState] corresponds to the state that this bloc can emit.
class FbLoginBloc extends Bloc<FbLoginEvent, FacebookLoginState> {
  bool isLoggedIn = false;
  var profileData;
  var facebookLogin = FacebookLogin();

  @override
  FacebookLoginInitialState get initialState => FacebookLoginInitialState();

  @override
  Stream<FacebookLoginState> mapEventToState(
    FbLoginEvent event,
  ) async* {
    if (event is FbLoginEvent) {
      yield FbLoginLoadingState();

      var facebookLoginResult = await _initiateFacebookLogin();

      print('FbLoginBloc FbLoginEvent facebookLoginResult.status ${facebookLoginResult.status}');
      switch (facebookLoginResult.status) {
        case FacebookLoginStatus.error:
          var appError = AppError(message: facebookLoginResult.errorMessage);
          yield FbLoginFailureState(error: appError);
          break;
        case FacebookLoginStatus.cancelledByUser:
          var appError = AppError(code: ErrorCode.Cancelled);
          yield FbLoginFailureState(error: appError);
          break;
        case FacebookLoginStatus.loggedIn:
          try{
            var graphResponse = await http.get(
                'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.width(400)&access_token=${facebookLoginResult.accessToken.token}');

            var profile = json.decode(graphResponse.body);
            print("FbLoginEvent FacebookLoginStatus.loggedIn profile $profile");

            yield FbLoginSuccessState(profile: profile);
          } catch(error){
            print('FacebookLoginStatus.loggedIn error $error');
            AppError appError = Utilities.getError(error);
            yield FbLoginFailureState(error: appError);
          }
          break;
      }
    }

    if (event is FbLoginSuccessEvent) {
      if (event.profileData != null && event.profileData.containsKey('id')) {
        _navigateToHomeScreen();
      }
    } else {
      _navigateToHomeScreen();
    }
  }

   Future<FacebookLoginResult> _initiateFacebookLogin() async {
    var facebookLoginResult = await facebookLogin.logIn(['email']);
    return facebookLoginResult;
  }

  /// Navigates the user to home screen clearing the stack.
  _navigateToHomeScreen() {
    Injector.resolve<AppRoutes>()
        .navigatorKey
        .currentState
        .pushNamedAndRemoveUntil(
            FacebookLoginScreen.routeName, (Route<dynamic> route) => false);
  }
}
