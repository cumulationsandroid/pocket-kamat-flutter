import 'package:flutter/foundation.dart';

import '../../base/base_event.dart';

/// Base class for the events that can be added to [LoginBloc].
abstract class FbLoginEvent extends BaseEvent {}

/// Instance of this class can be added to [LoginBloc] to login a particular user.
class FbLoginUserEvent extends FbLoginEvent {
  @override
  List<Object> get props => [];
}

/// Instance of this class can be added to [LoginBloc] to handle the screen navigation
/// after the user logs in.
class FbLoginSuccessEvent extends FbLoginEvent {
  /// profileData id of the user who is logged in.
  final Map<String, dynamic> profileData;

  FbLoginSuccessEvent({@required this.profileData});

  @override
  List<Object> get props => [profileData];
}
