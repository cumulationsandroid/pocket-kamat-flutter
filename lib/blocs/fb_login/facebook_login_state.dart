import 'package:flutter/foundation.dart';

import '../../base/base_state.dart';
import '../../common/error.dart';

/// Instance of this class is emitted by [LoginBloc].
abstract class FacebookLoginState extends BaseState {}

/// Instance of this class is emitted by [LoginBloc] when bloc is in initial state.
class FacebookLoginInitialState extends FacebookLoginState {
  @override
  List<Object> get props => [];
}

/// Instance of this class is emitted by [LoginBloc] when bloc is logging the user in.
class FbLoginLoadingState extends FacebookLoginState {
  @override
  List<Object> get props => [];
}

/// Instance of this class is emitted by [LoginBloc] when bloc has successfully logged in the user.
class FbLoginSuccessState extends FacebookLoginState {
  /// Represents profile of the successfully logged in user
  final dynamic profile;

  FbLoginSuccessState({@required this.profile});

  @override
  List<Object> get props => [];
}

/// Instance of this class is emitted by [LoginBloc] when bloc has to login an user.
class FbLoginFailureState extends FacebookLoginState {
  /// Represents any error occured while logging in the user.
  final AppError error;

  FbLoginFailureState({@required this.error});
  @override
  List<Object> get props => [];
}
