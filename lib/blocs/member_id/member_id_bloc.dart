import '../../base/base_bloc.dart';
import 'member_id_event.dart';
import 'member_id_state.dart';

/// This Bloc used to validate a given member id.
///
/// [MemberIdValidationEvent] corresponds to the appropriate events that can be added to this
/// bloc. [MemberIdValidationState] corresponds to the state that this bloc can emit.
class MemberIdValidationBloc
    extends BaseBloc<MemberIdValidationEvent, MemberIdValidationState> {
  @override
  MemberIdValidationState get initialState => MemberIdInitialState();

  @override
  Stream<MemberIdValidationState> mapEventToState(
      MemberIdValidationEvent event) async* {
    if (event is ValidateMemberIdEvent) {
      yield _validateMemberId(event);
    }
  }

  /// Returns an instance of [MemberIdEmptyState] if the member id present in [event] is empty,
  /// else returns an instance of [MemberIdValidState].
  MemberIdValidationState _validateMemberId(ValidateMemberIdEvent event) {
    if (event.memberId == null || event.memberId.trim().isEmpty) {
      return MemberIdEmptyState();
    } else {
      return MemberIdValidState();
    }
  }
}
