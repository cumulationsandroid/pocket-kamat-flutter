import 'package:flutter/foundation.dart';

import '../../base/base_event.dart';

/// Base class for the events that can be added to [MemberIdValidationBloc].
abstract class MemberIdValidationEvent extends BaseEvent {}

/// Instance of this class can be added to [MemberIdValidationBloc] to validate the [memberId].
class ValidateMemberIdEvent extends MemberIdValidationEvent {
  /// Member id that is to be validated.
  final String memberId;

  ValidateMemberIdEvent({@required this.memberId});
  @override
  List<Object> get props => [memberId];
}
