import '../../base/base_state.dart';


/// Base class for the states that are emitted by [MemberIdValidationBloc] when [ValidateMemberIdEvent] event is added to [MemberIdValidationBloc].
abstract class MemberIdValidationState extends BaseState {}

/// An instance of this class is emitted by [MemberIdValidationBloc] when bloc is in initial state while validating the member id.
class MemberIdInitialState extends MemberIdValidationState {
  @override
  List<Object> get props => [];
}

/// An instance of this class is emitted by [MemberIdValidationBloc] when the member id is empty.
class MemberIdEmptyState extends MemberIdValidationState {
  @override
  List<Object> get props => [];
}

/// An instance of this class is emitted by [MemberIdValidationBloc] when the member id is valid.
class MemberIdValidState extends MemberIdValidationState {
  @override
  List<Object> get props => [];
}
