import '../../base/base_bloc.dart';
import 'password_event.dart';
import 'password_state.dart';

/// This Bloc used to validate a given password.
///
/// [PasswordValidationEvent] corresponds to the appropriate events that can be added to this
/// bloc. [PasswordValidationState] corresponds to the state that this bloc can emit.
class PasswordValidationBloc
    extends BaseBloc<PasswordValidationEvent, PasswordValidationState> {
  @override
  PasswordValidationState get initialState => PasswordInitialState();

  @override
  Stream<PasswordValidationState> mapEventToState(
      PasswordValidationEvent event) async* {
    if (event is ValidatePasswordEvent) {
      yield _validatePassword(event);
    }
  }

  /// Returns an instance of [PasswordEmptyState] if the password present in [event] is empty,
  /// else returns an instance of [PasswordValidState].
  PasswordValidationState _validatePassword(ValidatePasswordEvent event) {
    if (event.password == null || event.password.trim().isEmpty) {
      return PasswordEmptyState();
    } else {
      return PasswordValidState();
    }
  }
}
