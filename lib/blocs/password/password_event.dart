import 'package:flutter/foundation.dart';

import '../../base/base_event.dart';

/// Base class for the events that can be added to [PasswordValidationBloc].
abstract class PasswordValidationEvent extends BaseEvent {}

/// Instance of this class can be added to [PasswordValidationBloc] to validate the [password].
class ValidatePasswordEvent extends PasswordValidationEvent {
  /// Password that is to be validated.
  final String password;

  ValidatePasswordEvent({@required this.password});
  @override
  List<Object> get props => [password];
}
