import '../../base/base_state.dart';

/// Base class for the states that are emitted by [PasswordValidationBloc].
abstract class PasswordValidationState extends BaseState {}

/// An instance of this class is emitted by [PasswordValidationBloc] when bloc is in initial state while validating the password.
class PasswordInitialState extends PasswordValidationState {
  @override
  List<Object> get props => [];
}

/// An instance of this class is emitted by [PasswordValidationBloc] when the password is empty.
class PasswordEmptyState extends PasswordValidationState {
  @override
  List<Object> get props => [];
}

/// An instance of this class is emitted by [PasswordValidationBloc] when the password is valid.
class PasswordValidState extends PasswordValidationState {
  @override
  List<Object> get props => [];
}
