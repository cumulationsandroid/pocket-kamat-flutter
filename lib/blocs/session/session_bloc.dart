import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pocket_kamat/screens/login/facebook_login_screen.dart';
import 'package:rxdart/rxdart.dart';

import '../../base/base_bloc.dart';
import '../../common/constants.dart';
import '../../common/log.dart';
import '../../common/routes.dart';
import '../../common/toast_helper.dart';
import '../../injector.dart';
import '../../network/client.dart';
import '../../preferences.dart';
import 'session_event.dart';
import 'session_state.dart';

/// This Bloc used to handle the app session.
///
/// [SessionEvent] corresponds to the appropriate events that can be added to this
/// bloc. [SessionState] corresponds to the state that this bloc can emit.
class SessionBloc extends BaseBloc<SessionEvent, SessionState> {

  /// Holds the reference to [AppPreferences] provided by the [Injector].
  final AppPreferences _appPreferences = Injector.resolve<AppPreferences>();

  /// Holds the reference to [ToastHelper] provided by the [Injector].
  final ToastHelper _toastHelper = Injector.resolve<ToastHelper>();

  /// Holds the reference to [AppNetworkClient] provided by the [Injector].
  final AppNetworkClient _appNetworkClient =
      Injector.resolve<AppNetworkClient>();

  /// Refers to the session timeout in seconds.
  int _timeoutInSeconds = -1;

  /// Holds the reference to the current active timer.
  Timer _timer;

  /// This flag is set to true if the app is in background and the session is timed out
  /// and a message is to be shown when the user opens the app from background.
  bool shouldShowSessionTimeoutMessage = false;

  /// Refers to the current app state.
  ///
  /// This is needed as the session timeout message should be shown only when the app is
  /// in foreground state.
  AppLifecycleState currentAppState = AppLifecycleState.resumed;

  @override
  SessionState get initialState => SessionState.initial();

  /// Initialises new timer for [_timeoutInSeconds] seconds and assigns it to the
  /// [_timer] field.
  _initializeTimer() async {
    if (_timeoutInSeconds == -1) {
      // Fetching the client specified session timeout
        _timeoutInSeconds = Constants.defaultSessionTimeout * 60;
    }

    Log.d("Session started - ${DateTime.now().toLocal().toIso8601String()}");

    // Creating a new timer.
    _timer = Timer.periodic(
      Duration(seconds: 1),
      (timer) {
        if (timer.tick >= _timeoutInSeconds) {
          add(SessionTimeoutEvent());
        }
      },
    );
  }

  /// Perfoms the log out activites.
  ///
  /// Clears push notifications from database.
  /// Cancels all active/pending network calls.
  /// Clears tokens, member id and frequent searches from preferences.
  /// Navigates user to login screen.
  /// Shows timeout message if the app is if foreground state.
  _logOutUser({didUserLogout = false}) async {
    Log.d("Session ended - ${DateTime.now().toLocal().toIso8601String()}");
    if (_timer != null) _timer.cancel();
    // Cancel all api requests
    _appNetworkClient.cancelAllRequests();

    // Clear tokens and databases
    await clearUserPreferencesAndDatabase();

    // Navigate user to Login screen.
    _navigateToLoginScreen();

    if (!didUserLogout ?? true) {
      /// Need to show session timeout message as the user has not logged out.
      /// Should show the message only if the app is in foreground.
      /// If the app is not in foreground, the message should be shown when the user
      /// opens the app from background.

      if (currentAppState == AppLifecycleState.resumed) {
        _toastHelper.showToast(msg: "Session timed out");
        shouldShowSessionTimeoutMessage = false;
      } else {
        shouldShowSessionTimeoutMessage = true;
      }
    } else {
      // No need to show session timeout message as the user has logged out.
      shouldShowSessionTimeoutMessage = false;

      _toastHelper.showToast(msg: "Logged out successfully");
    }
  }

  /// Navigates to Login screen clearing the stack.
  _navigateToLoginScreen() {
    Injector.resolve<AppRoutes>()
        .navigatorKey
        .currentState
        .pushNamedAndRemoveUntil(
            FacebookLoginScreen.routeName, (Route<dynamic> route) => false);
  }

  /// ReInitialises the timer whenever user performs any action in the app.
  _onUserAction() {
    if (_timer != null && !_timer.isActive) {
      // User logged out
      return;
    }

    if (_timer != null) _timer.cancel();
    _initializeTimer();
  }

  @override
  Stream<SessionState> transformEvents(Stream<SessionEvent> events,
      Stream<SessionState> Function(SessionEvent) next) {
    try {
      return (events as PublishSubject<SessionEvent>)
          .debounceTime(
            Duration(milliseconds: 500),
          )
          .switchMap(next);
    } catch (e) {
      return super.transformEvents(events, next);
    }
  }

  /// Updates the [currentAppState] with the current app state.
  ///
  /// Shows pending timeout message if the app is in foreground state.
  _updateAppState(AppLifecycleState state) {
    currentAppState = state;

    /// Need to show session timeout message as the user has not logged out.
    /// Should show the session timeout message if there pending session
    /// timeout message that needs to be shown when the app comes back from
    /// background.

    if (currentAppState == AppLifecycleState.resumed &&
        shouldShowSessionTimeoutMessage) {
      shouldShowSessionTimeoutMessage = false;
      _toastHelper.showToast(msg: "Session timed out");
    }
  }

  @override
  Stream<SessionState> mapEventToState(SessionEvent event) async* {
    if (event is UserActionEvent) {
      if (!state.isSessionTimedOut) {
        // Accept the user event only if the session is not timed out
        // Else it has to be first initialised by logging in
        _onUserAction();
        yield state.copyWith(isSessionTimedOut: false);
      }
    }

    if (event is LogoutEvent) {
      _logOutUser(didUserLogout: true);
      yield state.copyWith(isSessionTimedOut: true);
    }

    if (event is InitialiseSessionEvent) {
      yield state.copyWith(isSessionTimedOut: false);
      _initializeTimer();
    }

    if (event is UpdateAppStateEvent) {
      _updateAppState(event.state);
    }

    if (event is SessionTimeoutEvent) {
      _logOutUser(didUserLogout: false);
      yield state.copyWith(isSessionTimedOut: true);
    }
  }

  /// Clears the preferences and database.
  clearUserPreferencesAndDatabase() async {
    // Clear tokens in preference
    await _appPreferences.clear();

    Log.d("Cleared user preferences and database");
  }
}
