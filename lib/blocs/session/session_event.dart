import 'package:flutter/foundation.dart';
import 'package:flutter/scheduler.dart';

import '../../base/base_event.dart';

/// Base class for the events that can be added to [SessionBloc].
abstract class SessionEvent extends BaseEvent {}

/// Instance of this class can be added to [SessionBloc] to notifiy the bloc that user is using the app.
class UserActionEvent extends SessionEvent {
  @override
  List<Object> get props => [];
}

/// Instance of this class can be added to [SessionBloc] to initialise a new session.
class InitialiseSessionEvent extends SessionEvent {
  @override
  List<Object> get props => [];
}

/// Instance of this class can be added to [SessionBloc] to update the bloc with the current app state.
class UpdateAppStateEvent extends SessionEvent {
  /// Refers to the current app state.
  final AppLifecycleState state;

  UpdateAppStateEvent({@required this.state});

  @override
  List<Object> get props => [state];
}

/// Instance of this class can be added to [SessionBloc] to notify the bloc that session is timed out.
class SessionTimeoutEvent extends SessionEvent {
  @override
  List<Object> get props => [];
}

/// Instance of this class can be added to [SessionBloc] to notify the bloc when user has manually logged out.
class LogoutEvent extends SessionEvent {
  @override
  List<Object> get props => [];
}
