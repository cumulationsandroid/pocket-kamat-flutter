import 'package:flutter/foundation.dart';

import '../../base/base_state.dart';

/// Instance of this class is emitted by [SessionBloc].
class SessionState extends BaseState {
  /// This flag is set to true if the user session is timed out.
  bool isSessionTimedOut = false;

  SessionState({
    @required this.isSessionTimedOut,
  });

  factory SessionState.initial() => SessionState(
        isSessionTimedOut: true,
      );

  SessionState copyWith({
    bool isSessionTimedOut,
  }) {
    return SessionState(
      isSessionTimedOut: isSessionTimedOut ?? this.isSessionTimedOut,
    );
  }

  @override
  List<Object> get props => [isSessionTimedOut];
}
