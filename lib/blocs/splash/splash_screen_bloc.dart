import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pocket_kamat/screens/login/facebook_login_screen.dart';

import '../../base/base_bloc.dart';
import '../../common/constants.dart';
import '../../common/routes.dart';
import '../../injector.dart';
import '../../preferences.dart';
import '../session/session_bloc.dart';
import '../session/session_event.dart';
import 'bloc.dart';

/// This Bloc used to perform the initial navigation from [SplashScreen].
///
/// [SplashScreenEvent] corresponds to the appropriate events that can be added to this
/// bloc. [SplashScreenState] corresponds to the state that this bloc can emit.
class SplashScreenBloc extends BaseBloc<SplashScreenEvent, SplashScreenState> {
  /// Holds the reference to [AppPreferences] provided by the [Injector].
  ///
  /// This is used to check if the tokens and member id are present in the preferences.
  final AppPreferences _appPreferences = Injector.resolve<AppPreferences>();

  /// Holds the reference to [SessionBloc] provided by the [Injector].
  ///
  /// This is used to check if the session is timed out.
  final SessionBloc _sessionBloc = Injector.resolve<SessionBloc>();

  @override
  SplashScreenState get initialState => SplashScreenState.initial();

  @override
  Stream<SplashScreenState> mapEventToState(SplashScreenEvent event) async* {
    if (event is NavigateUserEvent) {
      // Fetch the access token from preferences
      final String accessToken = await _appPreferences.getAccessToken();

      // Fetch the refresh token from preferences
      final String refreshToken = await _appPreferences.getRefreshToken();

      await Future.delayed(
          Duration(
            seconds: Constants.splashScreenTimeout,
          ), () async {
        if (accessToken == null ||
            accessToken.isEmpty ||
            refreshToken == null ||
            refreshToken.isEmpty ||
            _sessionBloc.state.isSessionTimedOut) {
          await _sessionBloc.clearUserPreferencesAndDatabase();
            // Navigate to Login screen
            _navigateToLoginScreen();
        } else {
          // Navigate to main screen
          _sessionBloc.add(InitialiseSessionEvent());
//          _navigateToMainScreen();
        }
      });
    }
  }

  /// Navigates to the [LoginScreen] clearing the stack.
  _navigateToLoginScreen() {
    Injector.resolve<AppRoutes>()
        .navigatorKey
        .currentState
        .pushNamedAndRemoveUntil(
            FacebookLoginScreen.routeName, (Route<dynamic> route) => false);
  }
}
