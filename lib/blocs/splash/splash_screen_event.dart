import '../../base/base_event.dart';

/// Base class for the events that can be added to [SplashScreenBloc].
abstract class SplashScreenEvent extends BaseEvent {}

/// Instance of this class can be added to [SplashScreenBloc] to navigate the user to appropriate screen.
class NavigateUserEvent extends SplashScreenEvent {
  @override
  List<Object> get props => [];
}
