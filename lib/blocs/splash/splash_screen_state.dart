import 'package:flutter/foundation.dart';

import '../../base/base_state.dart';

/// Instance of this class is emitted by [SessionBloc].
class SplashScreenState extends BaseState {
  /// This flag is set to true if the user is logged in.
  bool isUserLoggedIn = false;

  /// This flag is set to true if the user has to be redirected to [OnBoardingScreen].
  bool shouldShowOnboardingScreen = false;

  SplashScreenState({
    @required this.isUserLoggedIn,
    @required this.shouldShowOnboardingScreen,
  });

  factory SplashScreenState.initial() => SplashScreenState(
        isUserLoggedIn: false,
        shouldShowOnboardingScreen: false,
      );

  SplashScreenState copyWith({
    bool isUserLoggedIn,
    bool shouldShowOnboardingScreen,
  }) {
    return SplashScreenState(
      isUserLoggedIn: isUserLoggedIn ?? this.isUserLoggedIn,
      shouldShowOnboardingScreen:
          shouldShowOnboardingScreen ?? this.shouldShowOnboardingScreen,
    );
  }

  @override
  List<Object> get props => [isUserLoggedIn, shouldShowOnboardingScreen];
}
