import 'package:flutter/material.dart';

/// Refers to all the colors used within the app.
class AppColors {
  static const primary100 = Color(0xFF003FEB);
  static get primary8 => const Color(0xFF003FEB).withOpacity(0.08);
  static get primary5 => const Color(0xFF003FEB).withOpacity(0.05);
  static const progressBar = Color(0xFF66BB6A);
  static get green0_12 => const Color(0xFF66BB6A).withOpacity(0.12);
  static const text100 = Color(0xFF282C3F);
  static get text85 => const Color(0xFF282C3F).withOpacity(0.85);
  static get text50 => const Color(0xFF282C3F).withOpacity(0.50);
  static const screenBackground = Color(0xFFECEEF4);
  static const countBadgeBackground = Color(0xFFF15340);
  static const progressBarBackground = Color(0xFFEEEEF0);
  static const success = Color(0xFF1B5E20);
  static const warning = Color(0xFFFF8F00);
  static const dividerColor = Color(0xFF373564);
  static get yellow0_03 => const Color(0xFFFF8F00).withOpacity(0.03);
  static const error = Color(0xFFB00020);
  static get red0_02 => const Color(0xFFB00020).withOpacity(0.02);
  static get participantBackground =>
      Color.fromARGB(0, 0, 63, 235).withOpacity(0.08);
  static get inProgressBackground =>
      Color.fromARGB(0, 245, 175, 57).withOpacity(0.03);
  static const inProgressTextColor = Color(0xFFFF8F00);
  static get completedBackground => Color(0xFF66BB6A).withOpacity(0.12);

  static const inputFieldOutline = Color(0xFFCECFDB);
  static const failureImageFooter = Color(0xFF95B4C9);
  static const transparent = Colors.transparent;
  static const white = Colors.white;
  static get disableText => Color.fromRGBO(44, 56, 73, 0.5);
  static get toolTipTextColor => Color(0xff1A237E);
  static const logoGradientBgColor = Color(0xffFFBD00);
}
