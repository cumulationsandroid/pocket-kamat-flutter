import 'package:flutter/material.dart';

// Refers to all the constants used.
class Constants {
  static const supportedLocales = [
    Locale('en', 'US'),
    Locale('en', 'UK'),
    Locale('en', 'IN'),
    Locale('hi', 'IN'),
  ];

  // Database
  static const databaseName = 'zeomega_db.db';
  static const dbVersion = 1;

  // Network
  static const apiTimeout = 32000; // in millis
  static const authTokenRefreshBufferTime = 15; // in secs
  static const authTokenTimeout = 3600; // in secs

  // Screens
  static const maxOnboardingScreens = 3;
  static const defaultMinPasswordLength = 8;
  static const defaultMaxPasswordLength = 8;

  // Misc
  static const splashScreenTimeout = 2; // in seconds
  static const defaultLoginDomain = "zeomega.com/";

  // Api
  static const unAuthorizedStatusCode = 401;
  static const apiSuccessMessage = "success";
  static const itemsPerApiLimit = 10; // Limit parameter of api
  static const defaultMemberIdType = "ELIG";

  // Initails
  static const firstDateInDatePicker = "1900-01-01T00:00:00Z";
  static const endNoOfDaysInDatePicker =
      3650; // In days, added to the current date

  // Session
  static const defaultSessionTimeout = 5; // In minutes
}
