import 'package:equatable/equatable.dart';

/// Refers to any error that has occured in the app.
class AppError extends Equatable {
  /// Refers to the error message.
  ///
  /// Default error message is - "Something went wrong"
  String message;

  /// Refers to the error code.
  ///
  /// Default error message is - ErrorCode.UnKnown 
  ErrorCode code;

  AppError(
      {this.code = ErrorCode.UnKnown, this.message = "Something went wrong"}) {
    if (code == null) {
      this.code = ErrorCode.UnKnown;
    }
    if (message == null) {
      this.message = "Something went wrong";
    }
  }

  @override
  List<Object> get props => [code];
}

enum ErrorCode {
  UnKnown,
  NoInternet,
  Cancelled,

  // Login
  AuthenticationFailure,
  InvalidLoginCredentials,
  RefreshTokenFailure,

  // Search Assessments api response codes
  MemberNotFound,
  MemberInActive,
  NoAssessmentsFound,
  InvalidExtMemberId,
  NoQuestionsResponsesFound,

  // Search Documents api response codes
  NoDocumentsFound,

  // Search Enrolled Programs api response codes
  NoProgramsFound,

  LabCodeNotFound,

  // Participant Api response code
  NoParticipantsFound,

  // Search Acitivities api response codes
  NoActivitiesFound,
  UsersNotFound,

  // Search Alerts api response codes
  NoAlertsFound,

  // Search Care Reminder api response codes
  NoCareRemindersFound,

  InvalidParameterValue,

  // Search Clinical Indicators api response code
  InsufficientParameters,
  InvalidClinicalIndicatorType,
  ClinicalIndicatorsNotFound,
  JsonValidator,
  InvalidExtMemberIdFormat,

  // Search Activities api response codes
  NoAppointmentsFound,

  InCorrectCurrentPassword,
  EnteredOtpIsNotValid
}
