/// Refers to all the fonts used within the app.
class AppFonts{
  static const regular = 'Proxima-Nova-Regular';
  static const semibold = 'Proxima-Nova-Semibold';
}