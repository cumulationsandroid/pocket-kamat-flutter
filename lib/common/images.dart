/// Refers to all paths of the images used within the app.
class AppImages {
  static const base = "assets/images";
  //logo
  static const appLogo = "$base/app_logo.png";

  // Splash Screen
  static const splashScreenLogoImage = "$base/splash_screen_logo.png";
  static const splashPoweredByImage = "$base/splash_powered_by.png";

  // Login Screen
  static const loginImage = "$base/login_image.png";

  // Login Screen background
  static const loginBgImage = "$base/login_background.png";
  // Facebook Login button
  static const facebookLoginButton = "$base/facebook_signin_button.png";

  // No internet screen
  static const noInternet = "$base/no_internet.png";

  //success image
  static const successImage = "$base/success_img.png";

  // No notifications image
  static const emptyNotifications = "$base/empty_notifications.png";
}
