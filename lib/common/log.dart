import 'package:logger/logger.dart';
import 'package:pocket_kamat/injector.dart';

/// A static in app logger class.
/// 
/// [logger] can be replaced with other logging libraries.
class Log {
  static Logger logger = Injector.resolve<Logger>();

  static d(dynamic message, {String tag}) {
    logger.d((tag != null) ? tag + message.toString() : message.toString());
  }

  static e(dynamic message, {String tag, String error, StackTrace stackTrace}) {
    logger.e((tag != null) ? tag + message.toString() : message.toString(),
        error, stackTrace);
  }
}
