import 'package:flutter/material.dart';

/// Provides the screen height and width of the phone in which the app is executed.
/// 
/// This is initialised in [SplashScreen] and provides the dimensions using [MediaQuery].
class PhoneScreenUtils {
  double _screenWidth = 0;
  double _screenHeight = 0;

  init(BuildContext context) {
    if (_screenHeight == 0) {
      _screenHeight = MediaQuery.of(context).size.height;
    }
    if (_screenWidth == 0) {
      _screenWidth = MediaQuery.of(context).size.width;
    }
  }

  /// Returns the width of the screen.
  double get screenWidth => _screenWidth;

  /// Returns the height of the screen.
  double get screenHeight => _screenHeight;

  /// Returns true if phone screen width is lesser than or equal to 375px.
  bool get isSmallScreen {
    return _screenWidth <= 375;
  }
}
