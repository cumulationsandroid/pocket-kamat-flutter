import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pocket_kamat/blocs/fb_login/facebook_login_bloc.dart';
import 'package:pocket_kamat/screens/login/facebook_login_screen.dart';

import '../blocs/splash/bloc.dart';
import '../injector.dart';
import '../screens/splash/splash_screen.dart';

/// Handles the screen navigations within the app.
class AppRoutes {
  /// This key is used to perform the screen navigations.
  final navigatorKey = new GlobalKey<NavigatorState>();

  /// Returns the route to invalid screen.
  CupertinoPageRoute invalidRoute(String name) {
    return CupertinoPageRoute(
        settings: RouteSettings(name: name),
        builder: (_) => Scaffold(
              body: Center(
                key: ValueKey('invalid_screen'),
                child: Text('No route defined for $name'),
              ),
            ));
  }

  /// Returns the route to appropriate screen based on the [settings.name].
  Route<dynamic> generateRoute(RouteSettings settings) {
    if (settings != null) {
      switch (settings.name) {
        case SplashScreen.routeName:
          return CupertinoPageRoute(
            settings: settings,
            builder: (_) => BlocProvider.value(
              value: Injector.resolve<SplashScreenBloc>(),
              child: SplashScreen(),
            ),
          );

        case FacebookLoginScreen.routeName:
          return CupertinoPageRoute<FacebookLoginScreen>(
            settings: settings,
            builder: (_) => BlocProvider.value(
              value: Injector.resolve<FbLoginBloc>(),
              child: FacebookLoginScreen(),
            ),
          );

        default:
          return invalidRoute(settings.name);
      }
    }
    return invalidRoute('invalid');
  }
}
