class AppSize {
  // Font sizes
  static const text12 = 12.0;
  static const text24 = 24.0;
  static const text20 = 20.0;
  static const text18 = 18.0;
  static const text16 = 16.0;
  static const text14 = 14.0;
  static const text10 = 10.0;

  // Margins
  static const margin103 = 103.0;
  static const margin70 = 70.0;
  static const margin52 = 52.0;
  static const margin48 = 48.0;
  static const margin40 = 40.0;
  static const margin32 = 32.0;
  static const margin30 = 30.0;
  static const margin24 = 24.0;
  static const margin20 = 20.0;
  static const margin18 = 18.0;
  static const margin16 = 16.0;
  static const margin14 = 14.0;
  static const margin13 = 13.0;
  static const margin12 = 12.0;
  static const margin8 = 8.0;
  static const margin7 = 7.0;
  static const margin6 = 6.0;
  static const margin4 = 4.0;
  static const margin3 = 3.0;
  static const margin28 = 28.0;

  // Padding
  static const padding52 = 52.0;
  static const padding44 = 44.0;
  static const padding40 = 40.0;
  static const padding32 = 32.0;
  static const padding24 = 24.0;
  static const padding20 = 20.0;
  static const padding18 = 18.0;
  static const padding16 = 16.0;
  static const padding15 = 15.0;
  static const padding14 = 14.0;
  static const padding13 = 13.0;
  static const padding12 = 12.0;
  static const padding23 = 23.0;
  static const padding8 = 8.0;
  static const padding2 = 2.0;
  static const padding4 = 4.0;
  static const padding9 = 9.0;

  // Line Heights
  static const lineHeight1_25 = 1.25;
  static const lineHeight1_20 = 1.20;
  static const lineHeight1_15 = 1.15;
  static const lineHeight1 = 1.0;

  // Icon size
  static const icon13_5 = 13.5;
  static const icon18 = 18.0;
  static const icon16 = 16.0;
  static const icon20 = 20.0;
  static const icon24 = 24.0;
  static const icon40 = 40.0;

  // Heights
  static const buttonHeight56 = 56.0;
  static const defaultAppBarHeight = 56;
  static const buttonHeight40 = 40.0;
  static const buttonHeight26 = 26.0;
  static const largeButtonHeight = 56.0;
  static const mediumButtonHeight = 40.0;
  static const smallButtonHeight = 26.0;
  static const height90 = 90.0;
  static const height50 = 50.0;
  static const height40 = 40.0;
  static const height30 = 50.0;
  static const height282 = 282.0;
  static const height35 = 35.0;
  static const height32 = 32.0;
  static const height48 = 48.0;
  static const failureImageFooterHeight = 32.0;
  static const height18 = 18.0;
  static const height28 = 28.0;
  static const height16 = 16.0;
  static const height116 = 116.0;
  static const height36 = 36.0;
  static const height115 = 115.0;
  static const height117 = 117.0;
  static const height158 = 158.0;
  static const height20 = 20.0;
  static const height380 = 380.0;
  static const height96 = 96.0;
  static const height113 = 113.0;

  // Widths
  static const buttonWidth166 = 166.0;
  static const buttonWidth156 = 156.0;
  static const buttonWidth65 = 65.0;
  static const largeButtonWidth = double.infinity;
  static const mediumButtonWidth = 156.0;
  static const smallButtonWidth = 65.0;
  static const width40 = 40.0;
  static const width35 = 35.0;
  static const width1 = 1.0;
  static const width28 = 28.0;
  static const width24 = 24.0;
  static const width32 = 32.0;
  static const width18 = 18.0;
  static const width16 = 16.0;
  static const width282 = 282.0;
  static const width222 = 222.0;

  // Button stroke
  static const stroke2 = 2.0;
  static const stroke1 = 1.0;

  // Radius
  static const radius4 = 4.0;
  static const selectionButtonBorderRadius = 100.0;

  // Misc
  static const pageIndicatorSize = 10.0;
  static const progressIndicatorSize = 45.0;

  // Card heights
  static const pendingAssessmentCardHeight120 = 120.0;
  static const completedAssessmentCardHeight72 = 72.0;
  static var profileCardHeight = 144.0;
  static const cardHeight271 = 271.0;

  // Progress bar height
  static const progressBarHeight6 = 6.0;
  static const circleProgressBarHeight52 = 52.0;

  //image height
  static const profileImageHeight40 = 40.0;
  static const imageSize64 = 64.0;
  static const imageSize40 = 40.0;
  static const homeHeight76 = 76.0;

  static const height56 = 56.0;

  static const height14 = 14.0;

  static const width90 = 86.06;

  static const padding6 = 6.0;

  //Radius
  static const rectangleRadius4 = 4.0;
  static const radius100 = 100.0;

  static const height330 = 330.0;

  static const buttonWidth40 = 40.0;

  //appbar elevation
  static const appBarElevation = 1.0;

  static const cardHeight72 = 72.0;

  static const height130 = 130.0;

  static const height120 = 120.0;

  static const height123 = 123.0;

  static const height134 = 134.0;

  static const margin5 = 5.0;

  static const margin37 = 37.0;

  static const height60 = 60.0;

  static var height132 = 132.0;

  static var height102 = 102.0;

  static var height24 = 24.0;

  static var width62 = 62.0;

  static var height288 = 288.0;

  static var height104 = 104.0;

  static var padding1 = 1.0;

  static var cardHeight80 = 80.0;

  static var height112 = 112.0;

  static var cardHeight68 = 68.0;

  static var cardHeight176 = 176.0;

  static var cardHeight40 = 40.0;

  static var cardHeight252 = 252.0;

  static var width80 = 80.0;

  static var width8 = 8.0;
  static var height8 = 8.0;

  static var width150 = 150.0;
  static var cardHeight85 = 85.0;

  static var cardHeight210 = 210.0;

  static var padding10 = 10.0;

  static var imageSize200 = 200.0;
}
