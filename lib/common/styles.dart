import 'package:flutter/material.dart';

import '../widgets/texts.dart';
import 'fonts.dart';
import 'size.dart';

/// Provides all the text styles used within the app.
class AppTextStyles {
  /// Returns the [TextStyle] based on the [scale] and optional [params].
  static TextStyle get(
    AppTextScale scale, {
    AppTextParams params = const AppTextParams(),
  }) {
    String fontFamily = AppFonts.regular;
    num fontSize = AppSize.text12;
    switch (scale) {
      case AppTextScale.overline:
        fontFamily = AppFonts.regular;
        fontSize = AppSize.text14;
        break;
      case AppTextScale.overline2:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text10;
        break;
      case AppTextScale.overline1:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text14;
        break;
      case AppTextScale.overline2:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text10;
        break;
      case AppTextScale.caption:
        fontFamily = AppFonts.regular;
        fontSize = AppSize.text12;
        break;
      case AppTextScale.body:
        fontFamily = AppFonts.regular;
        fontSize = AppSize.text16;
        break;
      case AppTextScale.headline:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text16;
        break;
      case AppTextScale.headline2:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text14;
        break;
      case AppTextScale.title1:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text20;
        break;
      case AppTextScale.title2:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text18;
        break;
      case AppTextScale.largeTitle:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text24;
        break;
      case AppTextScale.footer:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text10;
        break;
      case AppTextScale.footer2:
        fontFamily = AppFonts.regular;
        fontSize = AppSize.text10;
        break;
      case AppTextScale.test:
        fontFamily = AppFonts.regular;
        fontSize = AppSize.text10;
        break;
      case AppTextScale.caption1:
        fontFamily = AppFonts.semibold;
        fontSize = AppSize.text12;
        break;

      default:
        break;
    }
    return TextStyle(
      height: AppSize.lineHeight1_20,
      fontFamily: fontFamily,
      fontSize: fontSize,
      color: params.color,
      fontWeight: params.fontWeight,
    );
  }
}
