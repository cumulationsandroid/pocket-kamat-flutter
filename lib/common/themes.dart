import 'package:flutter/material.dart';

import '../widgets/texts.dart';
import 'colors.dart';
import 'size.dart';
import 'styles.dart';

/// Provides all the themes used in the app.
class AppTheme {
  /// Returns the main app theme.
  static ThemeData get() {
    return ThemeData(
      primaryColor: AppColors.primary100,
      accentColor: AppColors.primary100,
      splashColor: AppColors.transparent,
      
      textTheme: TextTheme(
        headline: AppTextStyles.get(AppTextScale.headline),
        title: AppTextStyles.get(AppTextScale.title1),
        subtitle: AppTextStyles.get(AppTextScale.title2),
        body1: AppTextStyles.get(AppTextScale.body),
        overline: AppTextStyles.get(AppTextScale.overline),
        button: AppTextStyles.get(AppTextScale.body),
        display1: AppTextStyles.get(AppTextScale.body),
      ),
    );
  }
}
