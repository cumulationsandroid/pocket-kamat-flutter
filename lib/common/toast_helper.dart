import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/foundation.dart';

/// Used to show a toast message.
class ToastHelper {
  /// Shows a toast message with the given [msg].
  showToast({@required String msg}) {
    if (msg != null && msg.isNotEmpty) {
      // Fluttertoast.showToast(msg: msg);
      BotToast.showText(text: msg);
    }
  }
}
