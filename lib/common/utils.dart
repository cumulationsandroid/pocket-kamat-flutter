import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/base_response_payload.dart';
import '../network/urls.dart';
import 'error.dart';

/// Contains the utility functions.
class Utilities {
  /// Returns true if [value] exists within (200, 300)
  ///
  /// Used for validating api responses.
  static bool isSuccessResponse(int value) {
    return (value != null && (value >= 200 && value < 300));
  }

  /// Returns an instance of [AppError] based on the [error] occured.
  static AppError getError(dynamic error, {ErrorCode code, String message}) {
    if (error is SocketException || error is IOException) {
      return AppError(code: ErrorCode.NoInternet);
    }

    if (error is DioError) {
      if (error.type == DioErrorType.DEFAULT &&
          error.error != null &&
          (error.error is SocketException || error.error is IOException)) {
        return AppError(code: ErrorCode.NoInternet);
      }

      if (error.type == DioErrorType.CANCEL) {
        return AppError(code: ErrorCode.Cancelled);
      }

      if ((error.type == DioErrorType.CONNECT_TIMEOUT ||
          error.type == DioErrorType.RECEIVE_TIMEOUT ||
          error.type == DioErrorType.SEND_TIMEOUT)) {
        return AppError();
      }
      if (error.type == DioErrorType.CANCEL) {
        return AppError(code: ErrorCode.Cancelled);
      }
    }
    return AppError(code: code, message: message);
  }

  /// Returns a string appending the base url and the [api] provided.
  static String getCompleteUrl(String api) {
      return Urls.baseUrl + api;
  }

  /// Returns the exception class string from the api error response [payload] provided.
  static String getExceptionClass(BaseResponsePayload payload) {
    if (payload?.response?.errors?.errorList != null &&
        payload.response.errors.errorList.isNotEmpty &&
        payload.response.errors.errorList[0].exceptionClass != null &&
        payload.response.errors.errorList[0].exceptionClass.isNotEmpty) {
      return payload.response.errors.errorList[0].exceptionClass;
    }

    if (payload?.response?.errors?.error != null &&
        payload.response.errors.error.exceptionClass != null &&
        payload.response.errors.error.exceptionClass.isNotEmpty) {
      return payload.response.errors.error.exceptionClass;
    }
    return "Unknown";
  }

  /// Returns member id query params with [memberId] as the value.
  static getMemberIdParam(String memberId) {
    if (memberId == null) {
      return "?" + "ext_member_id=";
    }

    return "?" + "ext_member_id=" + memberId;
  }

  /// Returns a string appending the [memberId] and [type] with a comma in between them.
  static appendMemberIdAndType(String memberId, String type) {
    if (memberId == null || type == null) {
      return "";
    }

    return type + ',' + memberId;
  }

  /// Returns true if [value] is either positive or negative decimal number.
  static bool isNumberValid(String value) {
    Pattern pattern = r'^[-+]?[0-9]\d*(\.\d+)?$';
    RegExp regex = RegExp(pattern);

    return regex.hasMatch(value);
  }

  /// Returns true if the [value] containing a domain.
  ///
  /// ```dart
  /// isContainingDomain('zeomega.com/member1') => true
  /// isContainingDomain('member1') => false
  /// ```
  static bool isContainingDomain(String value) {
    Pattern pattern = r'[a-z0-9]+\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?(\/)?$';
    RegExp regex = RegExp(pattern);

    return regex.hasMatch(value);
  }

  /// Compares date1 with date2
  /// returns -1 if date1 is before date2
  /// returns 0 if date1 is same as date2
  /// return 1 if date1 is after date2
  ///
  /// This compares year, month, date, hour, minute and seconds
  static int compareDates(DateTime date1, DateTime date2) {
    if (date1.year > date2.year) {
      return 1;
    } else if (date1.year < date2.year) {
      return -1;
    } else {
      // Years are equal
      if (date1.month > date2.month) {
        return 1;
      } else if (date1.month < date2.month) {
        return -1;
      } else {
        // Months are equal
        if (date1.day > date2.day) {
          return 1;
        } else if (date1.day < date2.day) {
          return -1;
        } else {
          // Dates are equal
          if (date1.hour > date2.hour) {
            return 1;
          } else if (date1.hour < date2.hour) {
            return -1;
          } else {
            // Hours are equal
            if (date1.minute > date2.minute) {
              return 1;
            } else if (date1.minute < date2.minute) {
              return -1;
            } else {
              // Minutes are equal
              if (date1.second > date2.second) {
                return 1;
              } else if (date1.second < date2.second) {
                return -1;
              } else {
                return 0;
              }
            }
          }
        }
      }
    }
  }

  /// Converts the Base64 string to Image
  static Image getImageFromBase64(String base64ImageString) {
    return Image.memory(base64Decode(base64ImageString));
  }
}
