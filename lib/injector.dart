import 'package:dio/dio.dart';
import 'package:kiwi/kiwi.dart';
import 'package:logger/logger.dart';
import 'package:package_info/package_info.dart';
import 'package:pocket_kamat/network/api_service/login_api_service.dart';
import 'package:pocket_kamat/widgets/app_loader.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'blocs/fb_login/facebook_login_bloc.dart';
import 'blocs/session/session_bloc.dart';
import 'blocs/splash/bloc.dart';
import 'common/phone_screen_utils.dart';
import 'common/routes.dart';
import 'common/toast_helper.dart';
import 'network/client.dart';
import 'preferences.dart';

/// Dependency injection class.
///
/// Stores and provides all the instances of the registered classes in the singleton class [Container]
/// provided by the [kiwi] library.
class Injector {
  /// Singleton class instance provided by the [kiwi] library.
  static Container container = Container();

  /// Configures the Injector.
  static setUp() async {
    await _configure();
  }

  /// Provides the instances of registered classes from the singleton class [Container].
  static final resolve = container.resolve;

  /// Clears all the instances of registered classes from the singleton class [Container].
  static clear() => container.clear();

  /// Stores the instances of the registered classes in to the singleton class [Container]
  /// provided by the [kiwi] library.
  static _configure() async {

    // AppRoutes
    container.registerSingleton((c) => AppRoutes());

    // Phone screen utils
    container.registerSingleton((c) => PhoneScreenUtils());

    // ToastHelper
    container.registerSingleton((c) => ToastHelper());

    // App Loader
    container.registerSingleton((c) => AppLoader());

    // Logger
    container.registerSingleton((c) => Logger(
          printer: PrettyPrinter(methodCount: 0, printEmojis: false),
        ));

    // Network Logger
    container.registerSingleton((c) => PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: true,
          compact: false,
        ));

    // Dio
    container.registerSingleton((c) => Dio());

    // App info
    final PackageInfo packageInfo = await PackageInfo.fromPlatform();
    container.registerSingleton((c) => packageInfo);

    // Shared Preferences
    final prefsInstance = await SharedPreferences.getInstance();
    container.registerSingleton((c) => prefsInstance);

    // App Preferences
    container.registerSingleton((c) => AppPreferences());

    // Network client
    container.registerSingleton((c) => AppNetworkClient());

    // Session bloc
    container.registerSingleton((c) => SessionBloc());

    // Splash Screen Bloc
    container.registerFactory((c) => SplashScreenBloc());

    // Facebook login Bloc
    container.registerFactory((c) => FbLoginBloc());

    // Login api service
    container.registerFactory((c) => LoginApiService());
  }
}
