import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../common/constants.dart';

/// App Localizations helper class.
///
/// This is used to load appropriate strings from the assets based on the current locale.
class AppLocalizations {
  /// Current locale of the app.
  final Locale locale;

  /// String assets based on the current locale.
  /// Key is the string key from the [Strings] file.
  /// Value is the actual string from the json file in assets/locale json folder.
  Map<String, String> _localizedStrings;

  AppLocalizations(
    this.locale, {
    this.isTest = false,
  });
  bool isTest = false;

  /// Returns an instance of [AppLocalizations] based on the [context].
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  /// Loads the string assets based on the [locale].
  Future<AppLocalizations> load() async {
    String jsonString =
        await rootBundle.loadString('assets/lang/${locale.languageCode}.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _localizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });

    return AppLocalizations(locale);
  }

  /// Returns an instance of [AppLocalizations] used for testing.
  Future<AppLocalizations> loadTest(Locale locale) async {
    return AppLocalizations(locale);
  }

  /// Returns the actual string value based on the string [key].
  String translate(String key) {
    if (isTest) return key;

    if (key == null) {
      return "NO_STRING_FOUND";
    }
    return _localizedStrings[key] ?? "NO_STRING_FOUND";
  }
}

/// Localization delate class based on the [AppLocalizations].
class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate({
    this.isTest = false,
  });
  final bool isTest;

  @override
  bool isSupported(Locale locale) {
    return Constants.supportedLocales.firstWhere((supportedLocale) {
          return supportedLocale.languageCode == locale.languageCode;
        }, orElse: null) !=
        null;
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    AppLocalizations localizations =
        new AppLocalizations(locale, isTest: isTest ?? false);

    if (!isTest ?? true) {
      await localizations.load();
    } else {
      await localizations.loadTest(locale);
    }

    return localizations;
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) => false;
}
