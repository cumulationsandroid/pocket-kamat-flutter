/// Contains the string key values.
///
/// These are the keys for the json files in the assets/locale folder.
class Strings {
  static const appTitle = "app_title";
  static const test = "test";
  static const skip = "skip";
  static const next = "next";
  static const cancel = "cancel";
  static const save = "save";
  static const value = "value";
  static const yourPasswordMustInclude = "your_password_must_include";
  static const atLeastEightCharacters = "at_least_eight_characters";
  static const atLeastOneNumberOrSpecialCharacter =
      "at_least_one_number_or_special_character";
  static const doNotUseYourNameEmailOrPhoneNumber =
      "do_not_use_your_name_email_or_phone_number";
  static const passwordDoNotMatch = "password_do_not_match";
  static const enterValidNewPassword = "enter_valid_new_password";
  static const enterValidCurrentPassword = "enter_valid_current_password";
  static const currentPasswordIsIncorrect = "current_password_is_incorrect";
  static const pleaseEnterValidValue = "please_enter_valid_value";
  static const pleaseEnterValue = "please_enter_value";
  static const datehint = "date_hint";
  static const comments = "comments";
  static const submit = "submit";
  static const pleaseEnterComment = "please_enter_comment";
  static const login = "login";
  static const password = "password";
  static const passwordChangedSuccessFully = "password_changed_successfully";
  static const youCanNowLoginUsingNewPassword =
      "you_can_now_login_with_new_password";
  static const forgotPassword = "forgot_password";
  static const createAnAccount = "create_an_account";
  static const currentPassword = "current_password";
  static const newPassword = "new_password";
  static const reEnterPassword = "re-enter_password";
  static const passwordCannotBeEmpty = "password_cannot_be_empty";
  static const loggingIn = "logging_in";
  static const failedToLogin = "failed_to_login";
  static const searchHere = "search_here";
  static const noInternet = "no_internet";
  static const somethingWentWrong = "something_went_wrong";
  static const somethingWentWrongPleaseTryAgain = "something_went_wrong_please_try_again";
  static const invalidMemIdOrPassword = "invalid_mem_id_or_password";
  static const pleaseWait = "please_wait";
  static const oopsSomethingWentWrong = "oops_something_went_wrong";
  static const tryAgain = "try_again";
  static const home = "home";
  static const survey = "survey";
  static const schedule = "schedule";
  static const profile = "profile";
  static const more = "more";
  static const pending = "pending";
  static const completed = "completed";
  static const youAreAllCaughtUp = "you_are_all_caught_up";
  static const startNow = "start_now";
  static const startDate = "Start_date";
  static const endDate = "End_date";
  static const completeNow = "complete_now";
  static const showing = "showing";
  static const of = "of";
  static const viewAll = "view_all";
  static const addComment = "add_comment";
  static const alerts = "alerts";
  static const couldNotConnectToInternetPleaseCheckYourNetworkSettings =
      "could_not_connect_to_internet_please_check_your_network_settings";
  static const retry = "retry";
  static const upcoming = "upcoming";
  static const enrolledPrograms = "enrolled_programs";
  static const messages = "messages";
  static const help = "help";
  static const settings = "settings";
  static const done = "done";
  static const filter = "filter";
  static const clearAll = "clear_all";
  static const apply = "apply";
  static const okay = "okay";
  static const complete = "complete";
  static const enterValidNumber = "enter_valid_number";
  static const addressInformation = "address_information";
  static const contactInformation = "contact_information";
  static const changePassword = "change_password";
  static const description = "description";
  static const logout = "logout";
  static const appVersion = "app_version";
  static const updatingResponse = "updating_response";
  static const selected = "selected";
  static const available = "available";
  static const pleaseSelectDate = "please_select_date";
  static var inProgress = "in_progress";
  static const loading = "loading";
  static const no = "no";
  static const yes = "yes";
  static const responseCannotBeEmpty = "response_cannot_be_empty";
  static const normal = "normal";
  static const search_here = "search_here";
  static const doYouWantToExitTheApp = "do_you_want_to_exit_the_app";
  static const activityDate = "activity_date";
  static const comment = "comment";
  static const week = "week";
  static const month = "month";
  static const year = "year";
  static const history = "history";
  static const reportAProblem = "report_a_problem";
  static const faqS = "FAQ_s";
  static const helpCenter = "help_center";
  static const termsAndConditions = "terms_and_conditions";
  static const privacyStatement = "privacy_statement";
  static const otherLegal = "other_legal";

  static const notificationPreferences = "notification_preferences";
  static const about = "about";

  static const iWouldLikeToReceiveNotificationsRegardingTheFollowingTopics =
      "i_would_like_to_receive_notifications_regarding_the_following_topics";

  static const resetPassword = "reset_password";
  static const enterNewPassword = "enter_new_password";
  static const resendCode = "resend_code";
  static const verify = "verify";
  static const fourDigitCodeSentToMobile = "four_digit_code_sent_to_mobile";

  static const pleaseEnterTheFourDigitCodeSentToYourRegisteredMobileNumberAndEmailAddress =
      "please_enter_the_four_digit_code_sent_to_your_registered_mobile_number_and_email_address";

  static const reEnterNewPassword = "re_enter_new_password";

  static const pleaseEnterCode = "please_enter_code";
  static const invalidCode = "invalid_code";

  static const pleaseEnterPassword = "please_enter_password";

  static const verifyYourIdentity = "verify_your_identity";

  static const birthDate = "birth_date";
  static const pleaseSelectDOB = "please_select_dob";

  static const notifications = "notifications";
  static const noNotifications = "no_notifications";
  static const doYouWantToLogout = "do_you_want_to_logout";
  static String successfullyLoggedIn = "successfully_logged_in";
  static String successfullyLoggedOut = "successfully_logged_out";

  static String yourPasswordHasBeenChangedSuccessfully =
      "your_password_has_been_changed_successfully";
  static String commentsAddedSuccessfully = "comments_added_successfully";
  static const category = "category";
  static String facebookLoginCancelledByUser = "Facebook_login_cancelled_by_user";
}
