import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:pocket_kamat/locale/strings.dart';

import 'blocs/session/session_bloc.dart';
import 'blocs/session/session_event.dart';
import 'common/constants.dart';
import 'common/routes.dart';
import 'common/themes.dart';
import 'injector.dart';
import 'locale/app_localization.dart';
import 'screens/splash/splash_screen.dart';

void main() async {
  await init();

  runApp(
    BlocProvider<SessionBloc>(
      create: (_) => Injector.resolve<SessionBloc>(),
      child: MyApp(),
    ),
  );
}

init() async {
  // Initialising app dependencies
  WidgetsFlutterBinding.ensureInitialized();

  // Dependency Injection
  await Injector.setUp();
}

class MyApp extends StatefulWidget {
  const MyApp({
    Key key,
  }) : super(key: key);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  @override
  void didChangeAppLifecycleState(AppLifecycleState currentState) {
    if (currentState == AppLifecycleState.resumed) {
      // Restart session timer if the user comes from background.
      BlocProvider.of<SessionBloc>(context).add(UserActionEvent());
    }
    BlocProvider.of<SessionBloc>(context)
        .add(UpdateAppStateEvent(state: currentState));
    super.didChangeAppLifecycleState(currentState);
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerDown: (event) {
        BlocProvider.of<SessionBloc>(context).add(UserActionEvent());
      },
      child: Container(
        color: Colors.white,
        child: MaterialApp(
          title: Strings.appTitle,
          debugShowCheckedModeBanner: false,
          showPerformanceOverlay: false,
          theme: AppTheme.get(),
          builder: BotToastInit(),
          navigatorObservers: [BotToastNavigatorObserver()],
          supportedLocales: Constants.supportedLocales,
          localizationsDelegates: [
            AppLocalizationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          localeListResolutionCallback: (devicePreferredLocales, supportedLocales) {
            for (Locale locale in devicePreferredLocales) {
              if (supportedLocales.contains(locale)) {
                return locale;
              }
            }
            return supportedLocales.first;
          },
          onGenerateRoute: Injector.resolve<AppRoutes>().generateRoute,
          navigatorKey: Injector.resolve<AppRoutes>().navigatorKey,
          initialRoute: SplashScreen.routeName,
          // initialRoute: OnboardingScreen.routeName,
        ),
      ),
    );
  }
}
