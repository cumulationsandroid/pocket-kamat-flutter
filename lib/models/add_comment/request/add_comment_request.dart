import 'package:flutter/material.dart';

/// AddCommentRequest will be used as request body for adding comment for care reminders and this class will have all the required data.
class AddCommentRequest {
  AddCommentCareReminder careReminder;

  AddCommentRequest({
    @required this.careReminder,
  });

  factory AddCommentRequest.fromJson(Map<String, dynamic> json) =>
      AddCommentRequest(
        careReminder: json["careReminder"] == null
            ? null
            : AddCommentCareReminder.fromJson(json["careReminder"]),
      );

  Map<String, dynamic> toJson() => {
        "careReminder": careReminder == null ? null : careReminder.toJson(),
      };
}

class AddCommentCareReminder {
  String reasonCd;
  DateTime reportedDate;
  String comments;

  AddCommentCareReminder({
    @required this.reasonCd,
    @required this.reportedDate,
    @required this.comments,
  });

  factory AddCommentCareReminder.fromJson(Map<String, dynamic> json) =>
      AddCommentCareReminder(
        reasonCd: json["reasonCd"] == null ? null : json["reasonCd"],
        reportedDate: json["reportedDate"] == null
            ? null
            : DateTime.parse(json["reportedDate"]),
        comments: json["comments"] == null ? null : json["comments"],
      );

  Map<String, dynamic> toJson() => {
        "reasonCd": reasonCd == null ? null : reasonCd,
        "reportedDate":
            reportedDate == null ? null : reportedDate.toIso8601String(),
        "comments": comments == null ? null : comments,
      };
}

//class Domains {
//  List<Domain> domain;
//
//  Domains({
//    @required this.domain,
//  });
//
//  factory Domains.fromJson(Map<String, dynamic> json) => Domains(
//        domain: json["domain"] == null
//            ? null
//            : List<Domain>.from(json["domain"].map((x) => Domain.fromJson(x))),
//      );
//
//  Map<String, dynamic> toJson() => {
//        "domain": domain == null
//            ? null
//            : List<dynamic>.from(domain.map((x) => x.toJson())),
//      };
//}
//
//class Domain {
//  String domainCd;
//  String domainDesc;
//
//  Domain({
//    @required this.domainCd,
//    @required this.domainDesc,
//  });
//
//  factory Domain.fromJson(Map<String, dynamic> json) => Domain(
//        domainCd: json["domainCd"] == null ? null : json["domainCd"],
//        domainDesc: json["domainDesc"] == null ? null : json["domainDesc"],
//      );
//
//  Map<String, dynamic> toJson() => {
//        "domainCd": domainCd == null ? null : domainCd,
//        "domainDesc": domainDesc == null ? null : domainDesc,
//      };
//}
