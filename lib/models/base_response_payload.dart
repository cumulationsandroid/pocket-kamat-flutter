
import '../common/log.dart';
import 'profile_response/profile_content.dart';

enum ContentType {
  profileResponse,
}

class BaseResponsePayload {
  BaseResponse response;

  BaseResponsePayload({
    this.response,
  });

  factory BaseResponsePayload.fromJson(
          Map<String, dynamic> json, ContentType contentType) =>
      BaseResponsePayload(
        response: getResponse(json['response'], contentType),
      );

  Map<String, dynamic> toJson() => {
        "response": response == null ? null : response,
      };

  static getResponse(Map<String, dynamic> json, ContentType contentType) {
    dynamic response;
    if (json != null) {
      try {
        response = BaseResponse.fromJson(json, contentType);
      } catch (e) {
        response = null;
      }
    }
    return response;
  }
}

class BaseResponse {
  String result;
  String message;
  dynamic content;
  Errors errors;

  BaseResponse({
    this.result,
    this.message,
    this.content,
    this.errors,
  });

  factory BaseResponse.fromJson(
      Map<String, dynamic> json, ContentType contentType) {
    return BaseResponse(
      result: json["result"] == null ? null : json["result"],
      message: json["message"] == null ? null : json["message"],
      content: getContent(json['content'], contentType),
      errors: getErrors(json['errors']),
    );
  }

  static dynamic getContent(
      Map<String, dynamic> json, ContentType contentType) {
    dynamic content;
    if (contentType != null && json != null) {
      try {
        if (contentType == ContentType.profileResponse) {
          content = ProfileContent.fromJson(json);
        }
      } catch (e) {
        Log.d("Failing to parse api content of type $contentType");
        Log.d("Error $e");
        content = null;
      }
    }
    return content;
  }

  static Errors getErrors(Map<String, dynamic> json) {
    Errors errors;

    if (json != null) {
      try {
        errors = Errors.fromJson(json);
      } catch (e) {
        errors = null;
      }
    }
    return errors;
  }
}

class Errors {
  List<Error> errorList;
  Error error;

  Errors({
    this.errorList,
    this.error,
  });

  factory Errors.fromJson(Map<String, dynamic> json) => Errors(
        errorList: getErrorList(json['error']),
        error: getError(json['error']),
      );

  Map<String, dynamic> toJson() => {
        "error": errorList == null
            ? null
            : List<dynamic>.from(errorList.map((x) => x.toJson())),
      };

  static List<Error> getErrorList(dynamic json) {
    List<Error> error;

    if (json != null &&
        (json is List<Map<String, dynamic>> || json is List<dynamic>)) {
      try {
        error = List<Error>.from(json.map((x) => Error.fromJson(x)));
      } catch (e) {
        error = null;
      }
    }
    return error;
  }

  static Error getError(dynamic json) {
    Error error;

    if (json != null && json is Map<String, dynamic>) {
      try {
        error = Error.fromJson(json);
      } catch (e) {
        error = null;
      }
    }
    return error;
  }
}

class Error {
  String code;
  String message;
  String exceptionClass;

  Error({
    this.code,
    this.message,
    this.exceptionClass,
  });

  factory Error.fromJson(Map<String, dynamic> json) => Error(
        code: json["code"] == null ? null : json["code"],
        message: json["message"] == null ? null : json["message"],
        exceptionClass:
            json["exception_class"] == null ? null : json["exception_class"],
      );

  Map<String, dynamic> toJson() => {
        "code": code == null ? null : code,
        "message": message == null ? null : message,
        "exception_class": exceptionClass == null ? null : exceptionClass,
      };
}
