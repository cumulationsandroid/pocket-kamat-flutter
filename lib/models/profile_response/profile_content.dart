class ProfileContent {
  ProfileMember member;

  ProfileContent({
    this.member,
  });

  factory ProfileContent.fromJson(Map<String, dynamic> json) => ProfileContent(
        member: json["MEMBER"] == null
            ? null
            : ProfileMember.fromJson(json["MEMBER"]),
      );

  Map<String, dynamic> toJson() => {
        "MEMBER": member == null ? null : member.toJson(),
      };
}

class ProfileMember {
  EpisodesUri episodesUri;
  Coverages coverages;
  Emails emails;
  Addresses addresses;
  Languages languages;
  ExtMemberIds extMemberIds;
  Demographic demographic;
  Phones phones;

  ProfileMember({
    this.episodesUri,
    this.coverages,
    this.emails,
    this.addresses,
    this.languages,
    this.extMemberIds,
    this.demographic,
    this.phones,
  });

  factory ProfileMember.fromJson(Map<String, dynamic> json) => ProfileMember(
        episodesUri: json["EPISODES_URI"] == null
            ? null
            : EpisodesUri.fromJson(json["EPISODES_URI"]),
        coverages: json["COVERAGES"] == null
            ? null
            : Coverages.fromJson(json["COVERAGES"]),
        emails: json["EMAILS"] == null ? null : Emails.fromJson(json["EMAILS"]),
        addresses: json["ADDRESSES"] == null
            ? null
            : Addresses.fromJson(json["ADDRESSES"]),
        languages: json["LANGUAGES"] == null
            ? null
            : Languages.fromJson(json["LANGUAGES"]),
        extMemberIds: json["EXT_MEMBER_IDS"] == null
            ? null
            : ExtMemberIds.fromJson(json["EXT_MEMBER_IDS"]),
        demographic: json["DEMOGRAPHIC"] == null
            ? null
            : Demographic.fromJson(json["DEMOGRAPHIC"]),
        phones: json["PHONES"] == null ? null : Phones.fromJson(json["PHONES"]),
      );

  Map<String, dynamic> toJson() => {
        "EPISODES_URI": episodesUri == null ? null : episodesUri.toJson(),
        "COVERAGES": coverages == null ? null : coverages.toJson(),
        "EMAILS": emails == null ? null : emails.toJson(),
        "ADDRESSES": addresses == null ? null : addresses.toJson(),
        "LANGUAGES": languages == null ? null : languages.toJson(),
        "EXT_MEMBER_IDS": extMemberIds == null ? null : extMemberIds.toJson(),
        "DEMOGRAPHIC": demographic == null ? null : demographic.toJson(),
        "PHONES": phones == null ? null : phones.toJson(),
      };
}

class Addresses {
  List<Address> address;

  Addresses({
    this.address,
  });

  factory Addresses.fromJson(Map<String, dynamic> json) => Addresses(
        address: json["ADDRESS"] == null
            ? null
            : List<Address>.from(
                json["ADDRESS"].map((x) => Address.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "ADDRESS": address == null
            ? null
            : List<dynamic>.from(address.map((x) => x.toJson())),
      };
}

class Address {
  String modifiedByUser;
  String stateDesc;
  String countryDesc;
  String stateCd;
  String countryCd;
  bool recordActive;
  int jivaMbrAddrId;
  String addressTypeCd;
  String county;
  int zip;
  String address1;
  DateTime modifiedDt;
  String addressTypeDesc;
  String city;
  String modifiedByName;
  bool preferredAddrFlag;
  DateTime createdDt;

  Address({
    this.modifiedByUser,
    this.stateDesc,
    this.countryDesc,
    this.stateCd,
    this.countryCd,
    this.recordActive,
    this.jivaMbrAddrId,
    this.addressTypeCd,
    this.county,
    this.zip,
    this.address1,
    this.modifiedDt,
    this.addressTypeDesc,
    this.city,
    this.modifiedByName,
    this.preferredAddrFlag,
    this.createdDt,
  });

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        modifiedByUser:
            json["MODIFIED_BY_USER"] == null ? null : json["MODIFIED_BY_USER"],
        stateDesc: json["STATE_DESC"] == null ? null : json["STATE_DESC"],
        countryDesc: json["COUNTRY_DESC"] == null ? null : json["COUNTRY_DESC"],
        stateCd: json["STATE_CD"] == null ? null : json["STATE_CD"],
        countryCd: json["COUNTRY_CD"] == null ? null : json["COUNTRY_CD"],
        recordActive:
            json["RECORD_ACTIVE"] == null ? null : json["RECORD_ACTIVE"],
        jivaMbrAddrId:
            json["JIVA_MBR_ADDR_ID"] == null ? null : json["JIVA_MBR_ADDR_ID"],
        addressTypeCd:
            json["ADDRESS_TYPE_CD"] == null ? null : json["ADDRESS_TYPE_CD"],
        county: json["COUNTY"] == null ? null : json["COUNTY"],
        zip: json["ZIP"] == null ? null : json["ZIP"],
        address1: json["ADDRESS1"] == null ? null : json["ADDRESS1"],
        modifiedDt: json["MODIFIED_DT"] == null
            ? null
            : DateTime.parse(json["MODIFIED_DT"]),
        addressTypeDesc: json["ADDRESS_TYPE_DESC"] == null
            ? null
            : json["ADDRESS_TYPE_DESC"],
        city: json["CITY"] == null ? null : json["CITY"],
        modifiedByName:
            json["MODIFIED_BY_NAME"] == null ? null : json["MODIFIED_BY_NAME"],
        preferredAddrFlag: json["PREFERRED_ADDR_FLAG"] == null
            ? null
            : json["PREFERRED_ADDR_FLAG"],
        createdDt: json["CREATED_DT"] == null
            ? null
            : DateTime.parse(json["CREATED_DT"]),
      );

  Map<String, dynamic> toJson() => {
        "MODIFIED_BY_USER": modifiedByUser == null ? null : modifiedByUser,
        "STATE_DESC": stateDesc == null ? null : stateDesc,
        "COUNTRY_DESC": countryDesc == null ? null : countryDesc,
        "STATE_CD": stateCd == null ? null : stateCd,
        "COUNTRY_CD": countryCd == null ? null : countryCd,
        "RECORD_ACTIVE": recordActive == null ? null : recordActive,
        "JIVA_MBR_ADDR_ID": jivaMbrAddrId == null ? null : jivaMbrAddrId,
        "ADDRESS_TYPE_CD": addressTypeCd == null ? null : addressTypeCd,
        "COUNTY": county == null ? null : county,
        "ZIP": zip == null ? null : zip,
        "ADDRESS1": address1 == null ? null : address1,
        "MODIFIED_DT": modifiedDt == null ? null : modifiedDt.toIso8601String(),
        "ADDRESS_TYPE_DESC": addressTypeDesc == null ? null : addressTypeDesc,
        "CITY": city == null ? null : city,
        "MODIFIED_BY_NAME": modifiedByName == null ? null : modifiedByName,
        "PREFERRED_ADDR_FLAG":
            preferredAddrFlag == null ? null : preferredAddrFlag,
        "CREATED_DT": createdDt == null ? null : createdDt.toIso8601String(),
      };
}

class Coverages {
  List<Coverage> coverage;

  Coverages({
    this.coverage,
  });

  factory Coverages.fromJson(Map<String, dynamic> json) => Coverages(
        coverage: json["COVERAGE"] == null
            ? null
            : List<Coverage>.from(
                json["COVERAGE"].map((x) => Coverage.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "COVERAGE": coverage == null
            ? null
            : List<dynamic>.from(coverage.map((x) => x.toJson())),
      };
}

class Coverage {
  DateTime coverageEffectiveDate;
  DateTime coverageTermedDate;
  String pcpFirstName;
  bool recordActive;
  String coverageLevelCd;
  String productCodeDesc;
  String insuredFirstName;
  String dataSourceCd;
  String insuredLastName;
  DateTime modifiedDt;
  String groupName;
  String insuredEnrollId;
  String modifiedByName;
  String coverageStatusCd;
  String coverageLevelDesc;
  DateTime createdDt;
  String benefitPlanName;
  String modifiedByUser;
  String groupId;
  String productTypeCd;
  String coverageStatusDesc;
  int jivaMbrCovgId;
  String pcpLastName;
  int dependentCd;
  String pcpId;
  bool isPrimary;
  String benefitPlanNo;
  String dependentDesc;
  String pcpEntityUri;

  Coverage({
    this.coverageEffectiveDate,
    this.coverageTermedDate,
    this.pcpFirstName,
    this.recordActive,
    this.coverageLevelCd,
    this.productCodeDesc,
    this.insuredFirstName,
    this.dataSourceCd,
    this.insuredLastName,
    this.modifiedDt,
    this.groupName,
    this.insuredEnrollId,
    this.modifiedByName,
    this.coverageStatusCd,
    this.coverageLevelDesc,
    this.createdDt,
    this.benefitPlanName,
    this.modifiedByUser,
    this.groupId,
    this.productTypeCd,
    this.coverageStatusDesc,
    this.jivaMbrCovgId,
    this.pcpLastName,
    this.dependentCd,
    this.pcpId,
    this.isPrimary,
    this.benefitPlanNo,
    this.dependentDesc,
    this.pcpEntityUri,
  });

  factory Coverage.fromJson(Map<String, dynamic> json) => Coverage(
        coverageEffectiveDate: json["COVERAGE_EFFECTIVE_DATE"] == null
            ? null
            : DateTime.parse(json["COVERAGE_EFFECTIVE_DATE"]),
        coverageTermedDate: json["COVERAGE_TERMED_DATE"] == null
            ? null
            : DateTime.parse(json["COVERAGE_TERMED_DATE"]),
        pcpFirstName:
            json["PCP_FIRST_NAME"] == null ? null : json["PCP_FIRST_NAME"],
        recordActive:
            json["RECORD_ACTIVE"] == null ? null : json["RECORD_ACTIVE"],
        coverageLevelCd: json["COVERAGE_LEVEL_CD"] == null
            ? null
            : json["COVERAGE_LEVEL_CD"],
        productCodeDesc: json["PRODUCT_CODE_DESC"] == null
            ? null
            : json["PRODUCT_CODE_DESC"],
        insuredFirstName: json["INSURED_FIRST_NAME"] == null
            ? null
            : json["INSURED_FIRST_NAME"],
        dataSourceCd:
            json["DATA_SOURCE_CD"] == null ? null : json["DATA_SOURCE_CD"],
        insuredLastName: json["INSURED_LAST_NAME"] == null
            ? null
            : json["INSURED_LAST_NAME"],
        modifiedDt: json["MODIFIED_DT"] == null
            ? null
            : DateTime.parse(json["MODIFIED_DT"]),
        groupName: json["GROUP_NAME"] == null ? null : json["GROUP_NAME"],
        insuredEnrollId: json["INSURED_ENROLL_ID"] == null
            ? null
            : json["INSURED_ENROLL_ID"],
        modifiedByName:
            json["MODIFIED_BY_NAME"] == null ? null : json["MODIFIED_BY_NAME"],
        coverageStatusCd: json["COVERAGE_STATUS_CD"] == null
            ? null
            : json["COVERAGE_STATUS_CD"],
        coverageLevelDesc: json["COVERAGE_LEVEL_DESC"] == null
            ? null
            : json["COVERAGE_LEVEL_DESC"],
        createdDt: json["CREATED_DT"] == null
            ? null
            : DateTime.parse(json["CREATED_DT"]),
        benefitPlanName: json["BENEFIT_PLAN_NAME"] == null
            ? null
            : json["BENEFIT_PLAN_NAME"],
        modifiedByUser:
            json["MODIFIED_BY_USER"] == null ? null : json["MODIFIED_BY_USER"],
        groupId: json["GROUP_ID"] == null ? null : json["GROUP_ID"],
        productTypeCd:
            json["PRODUCT_TYPE_CD"] == null ? null : json["PRODUCT_TYPE_CD"],
        coverageStatusDesc: json["COVERAGE_STATUS_DESC"] == null
            ? null
            : json["COVERAGE_STATUS_DESC"],
        jivaMbrCovgId:
            json["JIVA_MBR_COVG_ID"] == null ? null : json["JIVA_MBR_COVG_ID"],
        pcpLastName:
            json["PCP_LAST_NAME"] == null ? null : json["PCP_LAST_NAME"],
        dependentCd: json["DEPENDENT_CD"] == null ? null : json["DEPENDENT_CD"],
        pcpId: json["PCP_ID"] == null ? null : json["PCP_ID"],
        isPrimary: json["IS_PRIMARY"] == null ? null : json["IS_PRIMARY"],
        benefitPlanNo:
            json["BENEFIT_PLAN_NO"] == null ? null : json["BENEFIT_PLAN_NO"],
        dependentDesc:
            json["DEPENDENT_DESC"] == null ? null : json["DEPENDENT_DESC"],
        pcpEntityUri:
            json["PCP_ENTITY_URI"] == null ? null : json["PCP_ENTITY_URI"],
      );

  Map<String, dynamic> toJson() => {
        "COVERAGE_EFFECTIVE_DATE": coverageEffectiveDate == null
            ? null
            : "${coverageEffectiveDate.year.toString().padLeft(4, '0')}-${coverageEffectiveDate.month.toString().padLeft(2, '0')}-${coverageEffectiveDate.day.toString().padLeft(2, '0')}",
        "COVERAGE_TERMED_DATE": coverageTermedDate == null
            ? null
            : "${coverageTermedDate.year.toString().padLeft(4, '0')}-${coverageTermedDate.month.toString().padLeft(2, '0')}-${coverageTermedDate.day.toString().padLeft(2, '0')}",
        "PCP_FIRST_NAME": pcpFirstName == null ? null : pcpFirstName,
        "RECORD_ACTIVE": recordActive == null ? null : recordActive,
        "COVERAGE_LEVEL_CD": coverageLevelCd == null ? null : coverageLevelCd,
        "PRODUCT_CODE_DESC": productCodeDesc == null ? null : productCodeDesc,
        "INSURED_FIRST_NAME":
            insuredFirstName == null ? null : insuredFirstName,
        "DATA_SOURCE_CD": dataSourceCd == null ? null : dataSourceCd,
        "INSURED_LAST_NAME": insuredLastName == null ? null : insuredLastName,
        "MODIFIED_DT": modifiedDt == null ? null : modifiedDt.toIso8601String(),
        "GROUP_NAME": groupName == null ? null : groupName,
        "INSURED_ENROLL_ID": insuredEnrollId == null ? null : insuredEnrollId,
        "MODIFIED_BY_NAME": modifiedByName == null ? null : modifiedByName,
        "COVERAGE_STATUS_CD":
            coverageStatusCd == null ? null : coverageStatusCd,
        "COVERAGE_LEVEL_DESC":
            coverageLevelDesc == null ? null : coverageLevelDesc,
        "CREATED_DT": createdDt == null ? null : createdDt.toIso8601String(),
        "BENEFIT_PLAN_NAME": benefitPlanName == null ? null : benefitPlanName,
        "MODIFIED_BY_USER": modifiedByUser == null ? null : modifiedByUser,
        "GROUP_ID": groupId == null ? null : groupId,
        "PRODUCT_TYPE_CD": productTypeCd == null ? null : productTypeCd,
        "COVERAGE_STATUS_DESC":
            coverageStatusDesc == null ? null : coverageStatusDesc,
        "JIVA_MBR_COVG_ID": jivaMbrCovgId == null ? null : jivaMbrCovgId,
        "PCP_LAST_NAME": pcpLastName == null ? null : pcpLastName,
        "DEPENDENT_CD": dependentCd == null ? null : dependentCd,
        "PCP_ID": pcpId == null ? null : pcpId,
        "IS_PRIMARY": isPrimary == null ? null : isPrimary,
        "BENEFIT_PLAN_NO": benefitPlanNo == null ? null : benefitPlanNo,
        "DEPENDENT_DESC": dependentDesc == null ? null : dependentDesc,
        "PCP_ENTITY_URI": pcpEntityUri == null ? null : pcpEntityUri,
      };
}

class Demographic {
  String modifiedByUser;
  String genderCd;
  String genderDesc;
  String createdByName;
  bool behavioralHealthShareConsent;
  String lastName;
  String firstName;
  DateTime modifiedDt;
  String maritalStatusCd;
  DateTime birthDate;
  String acoName;
  bool healthRecordingShareConsent;
  String acoId;
  String createdByUser;
  String modifiedByName;
  DateTime createdDt;

  Demographic({
    this.modifiedByUser,
    this.genderCd,
    this.genderDesc,
    this.createdByName,
    this.behavioralHealthShareConsent,
    this.lastName,
    this.firstName,
    this.modifiedDt,
    this.maritalStatusCd,
    this.birthDate,
    this.acoName,
    this.healthRecordingShareConsent,
    this.acoId,
    this.createdByUser,
    this.modifiedByName,
    this.createdDt,
  });

  factory Demographic.fromJson(Map<String, dynamic> json) => Demographic(
        modifiedByUser:
            json["MODIFIED_BY_USER"] == null ? null : json["MODIFIED_BY_USER"],
        genderCd: json["GENDER_CD"] == null ? null : json["GENDER_CD"],
        genderDesc: json["GENDER_DESC"] == null ? null : json["GENDER_DESC"],
        createdByName:
            json["CREATED_BY_NAME"] == null ? null : json["CREATED_BY_NAME"],
        behavioralHealthShareConsent:
            json["BEHAVIORAL_HEALTH_SHARE_CONSENT"] == null
                ? null
                : json["BEHAVIORAL_HEALTH_SHARE_CONSENT"],
        lastName: json["LAST_NAME"] == null ? null : json["LAST_NAME"],
        firstName: json["FIRST_NAME"] == null ? null : json["FIRST_NAME"],
        modifiedDt: json["MODIFIED_DT"] == null
            ? null
            : DateTime.parse(json["MODIFIED_DT"]),
        maritalStatusCd: json["MARITAL_STATUS_CD"] == null
            ? null
            : json["MARITAL_STATUS_CD"],
        birthDate: json["BIRTH_DATE"] == null
            ? null
            : DateTime.parse(json["BIRTH_DATE"]),
        acoName: json["ACO_NAME"] == null ? null : json["ACO_NAME"],
        healthRecordingShareConsent:
            json["HEALTH_RECORDING_SHARE_CONSENT"] == null
                ? null
                : json["HEALTH_RECORDING_SHARE_CONSENT"],
        acoId: json["ACO_ID"] == null ? null : json["ACO_ID"],
        createdByUser:
            json["CREATED_BY_USER"] == null ? null : json["CREATED_BY_USER"],
        modifiedByName:
            json["MODIFIED_BY_NAME"] == null ? null : json["MODIFIED_BY_NAME"],
        createdDt: json["CREATED_DT"] == null
            ? null
            : DateTime.parse(json["CREATED_DT"]),
      );

  Map<String, dynamic> toJson() => {
        "MODIFIED_BY_USER": modifiedByUser == null ? null : modifiedByUser,
        "GENDER_CD": genderCd == null ? null : genderCd,
        "GENDER_DESC": genderDesc == null ? null : genderDesc,
        "CREATED_BY_NAME": createdByName == null ? null : createdByName,
        "BEHAVIORAL_HEALTH_SHARE_CONSENT": behavioralHealthShareConsent == null
            ? null
            : behavioralHealthShareConsent,
        "LAST_NAME": lastName == null ? null : lastName,
        "FIRST_NAME": firstName == null ? null : firstName,
        "MODIFIED_DT": modifiedDt == null ? null : modifiedDt.toIso8601String(),
        "MARITAL_STATUS_CD": maritalStatusCd == null ? null : maritalStatusCd,
        "BIRTH_DATE": birthDate == null
            ? null
            : "${birthDate.year.toString().padLeft(4, '0')}-${birthDate.month.toString().padLeft(2, '0')}-${birthDate.day.toString().padLeft(2, '0')}",
        "ACO_NAME": acoName == null ? null : acoName,
        "HEALTH_RECORDING_SHARE_CONSENT": healthRecordingShareConsent == null
            ? null
            : healthRecordingShareConsent,
        "ACO_ID": acoId == null ? null : acoId,
        "CREATED_BY_USER": createdByUser == null ? null : createdByUser,
        "MODIFIED_BY_NAME": modifiedByName == null ? null : modifiedByName,
        "CREATED_DT": createdDt == null ? null : createdDt.toIso8601String(),
      };
}

class Emails {
  List<Email> email;

  Emails({
    this.email,
  });

  factory Emails.fromJson(Map<String, dynamic> json) => Emails(
        email: json["EMAIL"] == null
            ? null
            : List<Email>.from(json["EMAIL"].map((x) => Email.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "EMAIL": email == null
            ? null
            : List<dynamic>.from(email.map((x) => x.toJson())),
      };
}

class Email {
  String modifiedByUser;
  String emailTypeCd;
  bool preferredEmailFlag;
  String emailTypeDesc;
  DateTime modifiedDt;
  String modifiedByName;
  bool recordActive;
  String emailId;
  DateTime createdDt;

  Email({
    this.modifiedByUser,
    this.emailTypeCd,
    this.preferredEmailFlag,
    this.emailTypeDesc,
    this.modifiedDt,
    this.modifiedByName,
    this.recordActive,
    this.emailId,
    this.createdDt,
  });

  factory Email.fromJson(Map<String, dynamic> json) => Email(
        modifiedByUser:
            json["MODIFIED_BY_USER"] == null ? null : json["MODIFIED_BY_USER"],
        emailTypeCd:
            json["EMAIL_TYPE_CD"] == null ? null : json["EMAIL_TYPE_CD"],
        preferredEmailFlag: json["PREFERRED_EMAIL_FLAG"] == null
            ? null
            : json["PREFERRED_EMAIL_FLAG"],
        emailTypeDesc:
            json["EMAIL_TYPE_DESC"] == null ? null : json["EMAIL_TYPE_DESC"],
        modifiedDt: json["MODIFIED_DT"] == null
            ? null
            : DateTime.parse(json["MODIFIED_DT"]),
        modifiedByName:
            json["MODIFIED_BY_NAME"] == null ? null : json["MODIFIED_BY_NAME"],
        recordActive:
            json["RECORD_ACTIVE"] == null ? null : json["RECORD_ACTIVE"],
        emailId: json["EMAIL_ID"] == null ? null : json["EMAIL_ID"],
        createdDt: json["CREATED_DT"] == null
            ? null
            : DateTime.parse(json["CREATED_DT"]),
      );

  Map<String, dynamic> toJson() => {
        "MODIFIED_BY_USER": modifiedByUser == null ? null : modifiedByUser,
        "EMAIL_TYPE_CD": emailTypeCd == null ? null : emailTypeCd,
        "PREFERRED_EMAIL_FLAG":
            preferredEmailFlag == null ? null : preferredEmailFlag,
        "EMAIL_TYPE_DESC": emailTypeDesc == null ? null : emailTypeDesc,
        "MODIFIED_DT": modifiedDt == null ? null : modifiedDt.toIso8601String(),
        "MODIFIED_BY_NAME": modifiedByName == null ? null : modifiedByName,
        "RECORD_ACTIVE": recordActive == null ? null : recordActive,
        "EMAIL_ID": emailId == null ? null : emailId,
        "CREATED_DT": createdDt == null ? null : createdDt.toIso8601String(),
      };
}

class EpisodesUri {
  List<String> episodeUri;

  EpisodesUri({
    this.episodeUri,
  });

  factory EpisodesUri.fromJson(Map<String, dynamic> json) => EpisodesUri(
        episodeUri: json["EPISODE_URI"] == null
            ? null
            : List<String>.from(json["EPISODE_URI"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "EPISODE_URI": episodeUri == null
            ? null
            : List<dynamic>.from(episodeUri.map((x) => x)),
      };
}

class ExtMemberIds {
  List<MemberId> memberId;

  ExtMemberIds({
    this.memberId,
  });

  factory ExtMemberIds.fromJson(Map<String, dynamic> json) => ExtMemberIds(
        memberId: json["MEMBER_ID"] == null
            ? null
            : List<MemberId>.from(
                json["MEMBER_ID"].map((x) => MemberId.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "MEMBER_ID": memberId == null
            ? null
            : List<dynamic>.from(memberId.map((x) => x.toJson())),
      };
}

class MemberId {
  String modifiedByUser;
  DateTime modifiedDt;
  String extMemberIdType;
  String extMemberId;
  String modifiedByName;
  DateTime createdDt;

  MemberId({
    this.modifiedByUser,
    this.modifiedDt,
    this.extMemberIdType,
    this.extMemberId,
    this.modifiedByName,
    this.createdDt,
  });

  factory MemberId.fromJson(Map<String, dynamic> json) => MemberId(
        modifiedByUser:
            json["MODIFIED_BY_USER"] == null ? null : json["MODIFIED_BY_USER"],
        modifiedDt: json["MODIFIED_DT"] == null
            ? null
            : DateTime.parse(json["MODIFIED_DT"]),
        extMemberIdType: json["EXT_MEMBER_ID_TYPE"] == null
            ? null
            : json["EXT_MEMBER_ID_TYPE"],
        extMemberId:
            json["EXT_MEMBER_ID"] == null ? null : json["EXT_MEMBER_ID"],
        modifiedByName:
            json["MODIFIED_BY_NAME"] == null ? null : json["MODIFIED_BY_NAME"],
        createdDt: json["CREATED_DT"] == null
            ? null
            : DateTime.parse(json["CREATED_DT"]),
      );

  Map<String, dynamic> toJson() => {
        "MODIFIED_BY_USER": modifiedByUser == null ? null : modifiedByUser,
        "MODIFIED_DT": modifiedDt == null ? null : modifiedDt.toIso8601String(),
        "EXT_MEMBER_ID_TYPE": extMemberIdType == null ? null : extMemberIdType,
        "EXT_MEMBER_ID": extMemberId == null ? null : extMemberId,
        "MODIFIED_BY_NAME": modifiedByName == null ? null : modifiedByName,
        "CREATED_DT": createdDt == null ? null : createdDt.toIso8601String(),
      };
}

class Languages {
  List<Language> language;

  Languages({
    this.language,
  });

  factory Languages.fromJson(Map<String, dynamic> json) => Languages(
        language: json["LANGUAGE"] == null
            ? null
            : List<Language>.from(
                json["LANGUAGE"].map((x) => Language.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "LANGUAGE": language == null
            ? null
            : List<dynamic>.from(language.map((x) => x.toJson())),
      };
}

class Language {
  String languageCd;
  String modifiedByUser;
  DateTime modifiedDt;
  String languageDesc;
  bool preferredLanguageFlag;
  String modifiedByName;
  bool recordActive;
  DateTime createdDt;

  Language({
    this.languageCd,
    this.modifiedByUser,
    this.modifiedDt,
    this.languageDesc,
    this.preferredLanguageFlag,
    this.modifiedByName,
    this.recordActive,
    this.createdDt,
  });

  factory Language.fromJson(Map<String, dynamic> json) => Language(
        languageCd: json["LANGUAGE_CD"] == null ? null : json["LANGUAGE_CD"],
        modifiedByUser:
            json["MODIFIED_BY_USER"] == null ? null : json["MODIFIED_BY_USER"],
        modifiedDt: json["MODIFIED_DT"] == null
            ? null
            : DateTime.parse(json["MODIFIED_DT"]),
        languageDesc:
            json["LANGUAGE_DESC"] == null ? null : json["LANGUAGE_DESC"],
        preferredLanguageFlag: json["PREFERRED_LANGUAGE_FLAG"] == null
            ? null
            : json["PREFERRED_LANGUAGE_FLAG"],
        modifiedByName:
            json["MODIFIED_BY_NAME"] == null ? null : json["MODIFIED_BY_NAME"],
        recordActive:
            json["RECORD_ACTIVE"] == null ? null : json["RECORD_ACTIVE"],
        createdDt: json["CREATED_DT"] == null
            ? null
            : DateTime.parse(json["CREATED_DT"]),
      );

  Map<String, dynamic> toJson() => {
        "LANGUAGE_CD": languageCd == null ? null : languageCd,
        "MODIFIED_BY_USER": modifiedByUser == null ? null : modifiedByUser,
        "MODIFIED_DT": modifiedDt == null ? null : modifiedDt.toIso8601String(),
        "LANGUAGE_DESC": languageDesc == null ? null : languageDesc,
        "PREFERRED_LANGUAGE_FLAG":
            preferredLanguageFlag == null ? null : preferredLanguageFlag,
        "MODIFIED_BY_NAME": modifiedByName == null ? null : modifiedByName,
        "RECORD_ACTIVE": recordActive == null ? null : recordActive,
        "CREATED_DT": createdDt == null ? null : createdDt.toIso8601String(),
      };
}

class Phones {
  List<Phone> phone;

  Phones({
    this.phone,
  });

  factory Phones.fromJson(Map<String, dynamic> json) => Phones(
        phone: json["PHONE"] == null
            ? null
            : List<Phone>.from(json["PHONE"].map((x) => Phone.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "PHONE": phone == null
            ? null
            : List<dynamic>.from(phone.map((x) => x.toJson())),
      };
}

class Phone {
  int jivaPhoneNumId;
  String modifiedByUser;
  DateTime modifiedDt;
  bool preferredPhoneFlag;
  String phoneTypeDesc;
  String modifiedByName;
  bool recordActive;
  String phoneTypeCd;
  int phoneNumber;
  DateTime createdDt;

  Phone({
    this.jivaPhoneNumId,
    this.modifiedByUser,
    this.modifiedDt,
    this.preferredPhoneFlag,
    this.phoneTypeDesc,
    this.modifiedByName,
    this.recordActive,
    this.phoneTypeCd,
    this.phoneNumber,
    this.createdDt,
  });

  factory Phone.fromJson(Map<String, dynamic> json) => Phone(
        jivaPhoneNumId: json["JIVA_PHONE_NUM_ID"] == null
            ? null
            : json["JIVA_PHONE_NUM_ID"],
        modifiedByUser:
            json["MODIFIED_BY_USER"] == null ? null : json["MODIFIED_BY_USER"],
        modifiedDt: json["MODIFIED_DT"] == null
            ? null
            : DateTime.parse(json["MODIFIED_DT"]),
        preferredPhoneFlag: json["PREFERRED_PHONE_FLAG"] == null
            ? null
            : json["PREFERRED_PHONE_FLAG"],
        phoneTypeDesc:
            json["PHONE_TYPE_DESC"] == null ? null : json["PHONE_TYPE_DESC"],
        modifiedByName:
            json["MODIFIED_BY_NAME"] == null ? null : json["MODIFIED_BY_NAME"],
        recordActive:
            json["RECORD_ACTIVE"] == null ? null : json["RECORD_ACTIVE"],
        phoneTypeCd:
            json["PHONE_TYPE_CD"] == null ? null : json["PHONE_TYPE_CD"],
        phoneNumber: json["PHONE_NUMBER"] == null
            ? null
            : json["PHONE_NUMBER"],
        createdDt: json["CREATED_DT"] == null
            ? null
            : DateTime.parse(json["CREATED_DT"]),
      );

  Map<String, dynamic> toJson() => {
        "JIVA_PHONE_NUM_ID": jivaPhoneNumId == null ? null : jivaPhoneNumId,
        "MODIFIED_BY_USER": modifiedByUser == null ? null : modifiedByUser,
        "MODIFIED_DT": modifiedDt == null ? null : modifiedDt.toIso8601String(),
        "PREFERRED_PHONE_FLAG":
            preferredPhoneFlag == null ? null : preferredPhoneFlag,
        "PHONE_TYPE_DESC": phoneTypeDesc == null ? null : phoneTypeDesc,
        "MODIFIED_BY_NAME": modifiedByName == null ? null : modifiedByName,
        "RECORD_ACTIVE": recordActive == null ? null : recordActive,
        "PHONE_TYPE_CD": phoneTypeCd == null ? null : phoneTypeCd,
        "PHONE_NUMBER": phoneNumber,
        "CREATED_DT": createdDt == null ? null : createdDt.toIso8601String(),
      };
}
