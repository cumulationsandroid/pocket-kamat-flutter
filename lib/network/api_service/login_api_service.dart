import 'package:either_option/either_option.dart';
import '../../common/constants.dart';
import '../../common/error.dart';
import '../../common/utils.dart';
import '../../injector.dart';
import '../../models/add_comment/request/add_comment_request.dart';
import '../../models/base_response_payload.dart';
import '../client.dart';
import '../urls.dart';
import '../../preferences.dart';

/// AddCommentApiService will be used to fire an API to Add Comment and it will respond to [AddCommentBloc] about the status of API.
class LoginApiService {
  AppNetworkClient _client = Injector.resolve<AppNetworkClient>();
  AppPreferences _appPreferences = Injector.resolve<AppPreferences>();

  Future<Either<bool, AppError>> addComment(String reasonCD,
      DateTime reportedDate, String comment) async {
    final extMemberId = await _appPreferences.getMemberId() ?? "";
    final response = await _client.put(Urls.baseUrl + "/10636",
        body: getRequestBody(reasonCD, reportedDate, comment));

    if (response == null) {
      return Right(AppError());
    }

    return response.fold((apiResponse) {
      final responsePayload =
          BaseResponsePayload.fromJson(apiResponse.data, ContentType.profileResponse);
      if (Utilities.isSuccessResponse(apiResponse.statusCode)) {
        if (responsePayload.response.result != null &&
            responsePayload.response.result.isNotEmpty &&
            responsePayload.response.result == Constants.apiSuccessMessage) {
          return Left(true);
        } else {
          return Right(AppError(
              code:
                  getErrorCode(Utilities.getExceptionClass(responsePayload))));
        }
      } else {
        return Right(AppError(
            code: getErrorCode(Utilities.getExceptionClass(responsePayload))));
      }
    }, (appError) {
      return Right(appError);
    });
  }

  ErrorCode getErrorCode(String exceptionClass) {
    if (exceptionClass == null || exceptionClass.isEmpty) {
      return ErrorCode.UnKnown;
    }

    if (exceptionClass == "MemberNotFound") {
      return ErrorCode.MemberNotFound;
    }

    if (exceptionClass == "MemberInActive") {
      return ErrorCode.MemberInActive;
    }

    return ErrorCode.UnKnown;
  }

  getRequestBody(String reasonCD, DateTime reportedDate, String comment) {
    return AddCommentRequest(
            careReminder: AddCommentCareReminder(
                reasonCd: reasonCD,
                reportedDate: reportedDate,
                comments: comment))
        .toJson();
  }
}
