import 'dart:io';

import 'package:dio/dio.dart';
import 'package:either_option/either_option.dart';
import 'package:flutter/foundation.dart' as Foundation;
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../common/constants.dart';
import '../common/error.dart';
import '../common/log.dart';
import '../common/utils.dart';
import '../injector.dart';

/// Network client helper used to perform the network operations.
///
/// Can call get, post and put apis.
/// Cancell all the active apis.
class AppNetworkClient {
  /// Holds the reference to the instance of [Dio] provided by the [Injector].
  ///
  /// This is library that performs the network requests.
  final Dio _dio = Injector.resolve<Dio>();

  /// Holds the reference to the instance of [PrettyDioLogger] provided by the [Injector].
  ///
  /// This is used for logging the network requests and responses.
  final PrettyDioLogger _loggerInterceptor =
      Injector.resolve<PrettyDioLogger>();

  /// List of cancel token of the all active apis.
  List<CancelToken> _cancelTokenList = [];

  AppNetworkClient({bool isTest = false}) {
    if (!(isTest ?? false)) {
      _dio.options.connectTimeout = Constants.apiTimeout;
      _dio.options.receiveTimeout = Constants.apiTimeout;
      _dio.options.sendTimeout = Constants.apiTimeout;

      // Logging
      loggingInterceptor(shouldEnable: true);
    }
  }

  /// Cancels all the tokens present in the [_cancelTokenList].
  cancelAllRequests() {
    if (_cancelTokenList != null && _cancelTokenList.isNotEmpty) {
      _cancelTokenList.forEach((cancelToken) {
        if (cancelToken != null && !cancelToken.isCancelled) {
          cancelToken.cancel();
        }
      });
    }
    _cancelTokenList = [];
    Log.d('AppNetworkClient - Cancelled all requests');
  }

  /// Adds the [PrettyDioLogger] logging interceptor to the interceptors list if [shouldEnable] flag is set to true.
  loggingInterceptor({bool shouldEnable = false}) {
    if (shouldEnable) {
      if (Foundation.kDebugMode) {
        if (!_dio.interceptors.contains(_loggerInterceptor)) {
          _dio.interceptors.add(_loggerInterceptor);
        }
      }
    } else {
      if (_dio.interceptors.contains(_loggerInterceptor)) {
        _dio.interceptors.remove(_loggerInterceptor);
      }
    }
  }

  /// Performs post network request
  ///
  /// [api] specifies the api end point of the request.
  /// [body] specifies the body of the request.
  /// [params] specifies the query params of the request.
  /// [options] specifies the headers of the request.
  /// enables refresh token interceptor if [shouldRefreshToken] is set to true.
  /// [cancelToken] specifies the cancel token of the request.
  Future<Either<Response, AppError>> post(
    String api, {
    Map<String, dynamic> body,
    Map<String, dynamic> params,
    Options options,
    bool shouldRefreshToken = true,
    CancelToken cancelToken,
  }) async {
    // Initialising request cancel token
    final requestCancelToken = cancelToken ?? CancelToken();
    _cancelTokenList.add(requestCancelToken);

    try {
      String url = Utilities.getCompleteUrl(api);

      Response result = await _dio.post(
        url,
        data: body,
        queryParameters: params,
        options: options,
        cancelToken: requestCancelToken,
      );

      _removeCancelToken(requestCancelToken);

      if (result != null) {
        return Left(result);
      }
    } catch (error) {
      _removeCancelToken(requestCancelToken);

      if (error is DioError && error.type == DioErrorType.RESPONSE) {
        return Left(error.response);
      }
      return Right(Utilities.getError(error));
    }
    _removeCancelToken(requestCancelToken);

    return Right(AppError());
  }

  /// Performs put network request
  ///
  /// [api] specifies the api end point of the request.
  /// [body] specifies the body of the request.
  /// [params] specifies the query params of the request.
  /// [options] specifies the headers of the request.
  /// enables refresh token interceptor if [shouldRefreshToken] is set to true.
  /// [cancelToken] specifies the cancel token of the request.
  Future<Either<Response, AppError>> put(
    String api, {
    Map<String, dynamic> body,
    Map<String, dynamic> params,
    Options options,
    bool shouldRefreshToken = true,
    CancelToken cancelToken,
  }) async {
    // Initialising request cancel token
    final requestCancelToken = cancelToken ?? CancelToken();
    _cancelTokenList.add(requestCancelToken);

    try {
      String url = Utilities.getCompleteUrl(api);

      Response result = await _dio.put(
        url,
        data: body,
        queryParameters: params,
        options: options,
        cancelToken: requestCancelToken,
      );

      _removeCancelToken(requestCancelToken);

      if (result != null) {
        return Left(result);
      }
    } catch (error) {
      _removeCancelToken(requestCancelToken);

      if (error is DioError && error.type == DioErrorType.RESPONSE) {
        return Left(error.response);
      }
      return Right(Utilities.getError(error));
    }
    _removeCancelToken(requestCancelToken);

    return Right(AppError());
  }

  /// Performs get network request
  ///
  /// [api] specifies the api end point of the request.
  /// [params] specifies the query params of the request.
  /// [options] specifies the headers of the request.
  /// enables refresh token interceptor if [shouldRefreshToken] is set to true.
  /// [cancelToken] specifies the cancel token of the request.
  Future<Either<Response, AppError>> get(
    String api, {
    Map<String, dynamic> params,
    Options options,
    bool shouldRefreshToken = true,
    CancelToken cancelToken,
  }) async {
    // Initialising request cancel token
    final requestCancelToken = cancelToken ?? CancelToken();
    _cancelTokenList.add(requestCancelToken);

    try {
      String url = Utilities.getCompleteUrl(api);

      Response result = await _dio.get(
        url,
        queryParameters: params,
        options: options,
        cancelToken: requestCancelToken,
      );

      _removeCancelToken(requestCancelToken);

      if (result != null) {
        return Left(result);
      }
    } catch (error) {
      _removeCancelToken(requestCancelToken);

      if (error is DioError && error.type == DioErrorType.RESPONSE) {
        return Left(error.response);
      }
      return Right(Utilities.getError(error));
    }
    _removeCancelToken(requestCancelToken);

    return Right(AppError());
  }

  /// Removes the [cancelTokenToBeRemoved] from the [_cancelTokenList].
  _removeCancelToken(CancelToken cancelTokenToBeRemoved) {
    _cancelTokenList
        .removeWhere((cancelToken) => cancelToken == cancelTokenToBeRemoved);
  }
}
