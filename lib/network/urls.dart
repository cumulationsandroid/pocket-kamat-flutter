/// Contains all the network URLs used in the app.
class Urls {
  static const baseUrl = "https://api-ecare.zeomega.com";

  // Auth token api
  static const authTokenApi = "/oauth2/token";
}
