import 'package:shared_preferences/shared_preferences.dart';

import 'injector.dart';

/// Stores and retrieves the app preferences.
class AppPreferences {
  /// Holds the reference to the instance of [SharedPreferences] provided by the [Injector].
  final SharedPreferences _preferences = Injector.resolve<SharedPreferences>();

  /// Returns the refresh token.
  Future<String> getRefreshToken() async {
    return _preferences.getString(AppPreferencesKeys.refreshToken);
  }

  /// Sets the [value] as the refresh token.
  Future<bool> setRefreshToken(String value) async {
    return _preferences.setString(AppPreferencesKeys.refreshToken, value);
  }

  /// Returns the access token.
  Future<String> getAccessToken() async {
    return _preferences.getString(AppPreferencesKeys.accessToken);
  }

  /// Sets the [value] as the access token.
  Future<bool> setAccessToken(String value) async {
    return _preferences.setString(AppPreferencesKeys.accessToken, value);
  }

  /// Returns the onboarding status.
  Future<bool> getShowOnboardingStatus() async {
    return _preferences.getBool(AppPreferencesKeys.showOnboarding);
  }

  /// Returns the list of frequent lab data  searches.
  Future<List<String>> getFrequentLabDataSearches() async {
    return _preferences.getStringList(AppPreferencesKeys.frequentLabDataSearches);
  }

  /// Sets the [frequentLabDatas] as the list of frequent lab data searches.
  Future<bool> setFrequentLabDataSearches(List<String> frequentLabDatas) async {
    return _preferences.setStringList(
        AppPreferencesKeys.frequentLabDataSearches, frequentLabDatas);
  }

  /// Returns the list of frequent vital searches.
  Future<List<String>> getFrequentVitalsSearches() async {
    return _preferences.getStringList(AppPreferencesKeys.frequentVitalsSearches);
  }

  /// Sets the [frequentVitals] as the list of frequent vital searches.
  Future<bool> setFrequentVitalsSearches(List<String> frequentVitals) async {
    return _preferences.setStringList(
        AppPreferencesKeys.frequentVitalsSearches, frequentVitals);
  }

  /// Sets the [value] as the onboarding status.
  Future<bool> setShowOnboardingStatus(bool value) async {
    return _preferences.setBool(AppPreferencesKeys.showOnboarding, value);
  }

  /// Sets the [value] as the member id.
  Future<bool> setMemberId(String value) async {
    return _preferences.setString(AppPreferencesKeys.memberId, value);
  }

  /// Returns the member id.
  Future<String> getMemberId() async {
    return _preferences.getString(AppPreferencesKeys.memberId);
  }

  // Clears the tokens, member id and frequent searches.
  Future clear() async {
    await setAccessToken(null);
    await setRefreshToken(null);
    await setMemberId(null);
    await setFrequentLabDataSearches(null);
    await setFrequentVitalsSearches(null);
  }
}

class AppPreferencesKeys {
  static const refreshToken = "ref_token";
  static const accessToken = "acc_token";
  static const showOnboarding = "show_onboarding";
  static const memberId = "member_id";
  static const frequentLabDataSearches = "frequent_lab_data_search";
  static const frequentVitalsSearches = "frequent_vitals_search";
}
