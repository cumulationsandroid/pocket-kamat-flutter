import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pocket_kamat/blocs/fb_login/facebook_login_bloc.dart';
import 'package:pocket_kamat/blocs/fb_login/facebook_login_event.dart';
import 'package:pocket_kamat/blocs/fb_login/facebook_login_state.dart';
import 'package:pocket_kamat/common/colors.dart';
import 'package:pocket_kamat/common/images.dart';
import 'package:pocket_kamat/common/size.dart';
import 'package:pocket_kamat/widgets/app_image.dart';

import '../../blocs/session/session_bloc.dart';
import '../../blocs/session/session_event.dart';
import '../../common/error.dart';
import '../../common/phone_screen_utils.dart';
import '../../common/toast_helper.dart';
import '../../injector.dart';
import '../../locale/app_localization.dart';
import '../../locale/strings.dart';
import '../../widgets/app_loader.dart';

/// Implementation of Login screen according to the designs.
///
/// This screen is visible to the user when the user trying to log into the app.
class FacebookLoginScreen extends StatefulWidget {
  /// Route name for this screen.
  static const routeName = "/FbLogin";

  @override
  _FacebookLoginScreenState createState() => _FacebookLoginScreenState();
}

class _FacebookLoginScreenState extends State<FacebookLoginScreen> {

  /// Holds the reference to the instance of [AppLoader] provided by the [Injector].
  ///
  /// This is needed to show the overlay loader while is login api is running.
  final AppLoader appLoader = Injector.resolve<AppLoader>();

  /// Holds the reference to the instance of [AppLoader] provided by the [Injector].
  ///
  /// This is needed to fetch the phone screen width and height.
  final PhoneScreenUtils phoneScreenUtils = Injector.resolve<PhoneScreenUtils>();

  /// Scaffold key of the login screen.
  final scaffoldKey = GlobalKey<ScaffoldState>(debugLabel: "FacebookLoginScreen Scaffold");

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        bloc: BlocProvider.of<FbLoginBloc>(context),
        listener: (ctx, state) {
          print('BlocListener state $state');
          if (state is FbLoginSuccessState) {
            appLoader.hide(context);
            BlocProvider.of<SessionBloc>(context).add(InitialiseSessionEvent());
            var profileData = jsonDecode(state.profile.toString());
            print('BlocListener FbLoginSuccessState profileData $profileData');
            BlocProvider.of<FbLoginBloc>(context)
                .add(FbLoginSuccessEvent(profileData: profileData));
            Injector.resolve<ToastHelper>().showToast(
                msg: AppLocalizations.of(context)
                    .translate(Strings.successfullyLoggedIn));
          }

          if (state is FbLoginFailureState) {
            print('BlocListener FbLoginFailureState error msg:${state.error.message} code:${state.error.code}');
            appLoader.hide(context);
            if (state.error != null &&
                state.error.code == ErrorCode.NoInternet) {
              Injector.resolve<ToastHelper>().showToast(
                  msg: AppLocalizations.of(context)
                      .translate(Strings.noInternet));
            } else if (state.error != null &&
                state.error.code == ErrorCode.Cancelled) {
              Injector.resolve<ToastHelper>().showToast(
                  msg: AppLocalizations.of(context)
                      .translate(Strings.facebookLoginCancelledByUser));
            } else {
              Injector.resolve<ToastHelper>().showToast(
                msg: AppLocalizations.of(context)
                    .translate(Strings.somethingWentWrongPleaseTryAgain),
              );
            }
          }
          if (state is FbLoginLoadingState) {
            // TODO: Show loader
            appLoader.show(context,
                message:
                    AppLocalizations.of(context).translate(Strings.loggingIn));
          }
        },
        child: SafeArea(
          key: ValueKey(FacebookLoginScreen.routeName),
          child: Scaffold(
            key: scaffoldKey,
            body: Container(
              constraints: BoxConstraints.expand(),     /*Width, height fill_parent*/
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: AlignmentDirectional.center,
                    child: Image.asset(
                      AppImages.loginBgImage,
                      fit: BoxFit.contain,
                      color: Colors.white.withOpacity(.7),
                      colorBlendMode: BlendMode.dstATop,
                    ),
                  ),
                  Align(
                    alignment: AlignmentDirectional.center,
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              gradient: RadialGradient(
                                  colors: [AppColors.logoGradientBgColor, Colors.transparent])),
                          height: AppSize.cardHeight252,
                          width: AppSize.cardHeight252,
                        ),
                        Container(
                          height: AppSize.height132,
                          width: AppSize.height132,
                          child: AppImage.asset(
                            AppImages.splashScreenLogoImage,
                            fit: BoxFit.contain,
                          ),
                        ),
                      ],
                    )

                  ),

                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(bottom: AppSize.buttonHeight56),
                      child: InkWell(
                        onTap: _facebookLoginOnClick,
                        child: Image.asset(
                          AppImages.facebookLoginButton,
                          height: AppSize.buttonHeight40,
                          fit: BoxFit.contain,
                        ),
                      )
                    ),
                  )
                ],
              ),
            )
          ),
        ));
  }

  _facebookLoginOnClick() async {
    BlocProvider.of<FbLoginBloc>(context).add(FbLoginUserEvent());
  }
}
