/// Keys used for the widgets used in [LoginScreen].
class LoginScreenKeys {
  static const memberIdField = "member_id_field";
  static const passwordField = "password_field";
  static const loginButton = "login_button";
  static const emptyMemberId = "empty_member_id";
  static const emptyPassword = "empty_password";
  static const loginLoading = "login_loading";
  static const loginFailure = "login_failure";
  static const loginScreen = "login_screen";
  static const forgotButton = "forgot_button";
}
