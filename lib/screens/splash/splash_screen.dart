import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pocket_kamat/common/colors.dart';

import '../../blocs/splash/splash_screen_bloc.dart';
import '../../blocs/splash/splash_screen_event.dart';
import '../../common/images.dart';
import '../../common/phone_screen_utils.dart';
import '../../common/size.dart';
import '../../injector.dart';
import '../../widgets/app_image.dart';
import 'splash_screen_keys.dart';

class SplashScreen extends StatefulWidget {
  static const routeName = "/splash";

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool isInit = true;

  @override
  void didChangeDependencies() async {
    if (isInit) {
      isInit = false;
      BlocProvider.of<SplashScreenBloc>(context).add(NavigateUserEvent());
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    Injector.resolve<PhoneScreenUtils>().init(context);
    return SafeArea(
      top: false,
      child: WillPopScope(
        onWillPop: () {
          return Future.value(false);
        },
        child: Scaffold(
          body: Container(
            constraints: BoxConstraints.expand(),
            color: Colors.white,
            key: ValueKey(SplashScreenKeys.splashScreen), // where to position the child
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      gradient: RadialGradient(
                          colors: [AppColors.logoGradientBgColor, Colors.white])),
                  height: AppSize.cardHeight210,
                  width: AppSize.cardHeight210,
                ),
                Container(
                  height: AppSize.height132,
                  width: AppSize.height132,
                  child: AppImage.asset(
                    AppImages.splashScreenLogoImage,
                    fit: BoxFit.contain,
                  ),
                ),
              ],
            )
          ),
        ),
      ),
    );
  }
}
