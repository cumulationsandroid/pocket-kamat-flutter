class SplashScreenKeys {
  static const clientLogo = "client_logo";
  static const clientLogoLoading = "client_logo_loading";
  static const clientLogoFailure = "client_logo_failure";
  static const splashScreen = "splash_screen";
}