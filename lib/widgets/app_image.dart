import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'app_loader.dart';

/// Provides a widget having the specified image.
class AppImage {
  /// Returns a widget with the image loaded from a specific URL.
  ///
  /// The URL from where the image has to be loaded is specified in [url] field.
  /// The shape of the image can be specified in [shape] field.
  /// The fit of the image can be specified in [fit] field.
  /// The key for the widget can be specified in [key] field.
  static network(
    String url, {
    Key key,
    BoxShape shape,
    BoxFit fit,
  }) {
    return ExtendedImage.network(
      url ?? "",
      cache: true,
      key: key,
      fit: (fit == null) ? BoxFit.none : fit,
      shape: (shape == null) ? BoxShape.rectangle : shape,
      loadStateChanged: (ExtendedImageState state) {
        if (state.extendedImageLoadState == LoadState.loading) {
          return AppLoadingWidget();
        }

        if (state.extendedImageLoadState == LoadState.failed) {
          return Icon(Icons.error_outline);
        }

        if (state.extendedImageLoadState == LoadState.completed) {
          return ExtendedRawImage(
            image: state.extendedImageInfo?.image,
            fit: fit,
          );
        }
        return Container();
      },
    );
  }

  /// Returns a widget with the image loaded from the assets.
  ///
  /// The path from where the image has to be loaded is specified in [path] field.
  /// The fit of the image can be specified in [fit] field.
  /// The key for the widget can be specified in [key] field.
  static asset(String path, {Key key, BoxFit fit}) {
    return Image.asset(
      path,
      key: key,
      fit: (fit == null) ? BoxFit.none : fit,
    );
  }
}
