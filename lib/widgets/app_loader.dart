import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../common/colors.dart';
import '../common/size.dart';
import '../locale/app_localization.dart';
import '../locale/strings.dart';
import 'texts.dart';

/// Type of the loaders used in the app.
enum AppLoaderType { fullscreen, overlay }

/// Provides the loaders that are used in the app.
class AppLoader {
  /// This flag is set to true if the loader is showing.
  bool _isShowing = false;

  /// Shows a loader based on the [type] default being [AppLoaderType.overlay]
  ///
  /// The loader message can be specified in [message] field.
  /// The loader key can be specified in [key] field.
  /// The loader can be dismissed if the [isDismissable] is marked true. By default its false.
  show(
    BuildContext context, {
    AppLoaderType type = AppLoaderType.overlay,
    String message,
    Key key,
    bool isDismissable = false,
  }) {
    hide(context);
    _isShowing = true;

    if (type == AppLoaderType.overlay) {
      showDialog(
        context: context,
        barrierDismissible: isDismissable,
        builder: (_) {
          return WillPopScope(
              onWillPop: () => Future.value(isDismissable),
              child: _AppOverlayLoader(
                message: message,
                key: key,
              ));
        },
      );
    } else {
      showDialog(
        context: context,
        barrierDismissible: isDismissable,
        builder: (_) {
          return WillPopScope(
              onWillPop: () => Future.value(isDismissable),
              child: AppFullScreenLoader(
                key: key,
              ));
        },
      );
    }
  }

  /// Hides the loader if showing.
  hide(BuildContext context) {
    if (_isShowing) {
      _isShowing = false;
      Navigator.of(context).pop();
    }
  }
}

/// Returns a loading widget used in the app.
class AppLoadingWidget extends StatelessWidget {
  Key widgetKey;
  AppLoadingWidget({Key key}) {
    this.widgetKey = key;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: AppSize.height35,
      width: AppSize.width35,
      key: widgetKey,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}

/// Non overlay full screen loader widget.
class AppFullScreenLoader extends StatelessWidget {
  const AppFullScreenLoader({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.screenBackground,
      height: double.infinity,
      width: double.infinity,
      child: Center(
        child: AppLoadingWidget(),
      ),
    );
  }
}

/// Overlay full screen loader widget.
class _AppOverlayLoader extends StatelessWidget {
  /// Loader message.
  String message;

  _AppOverlayLoader({Key key, this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (message == null || message.isEmpty) {
      message = AppLocalizations.of(context).translate(Strings.pleaseWait);
    }
    return Material(
      type: MaterialType.transparency,
      child: Container(
        color: Colors.white.withOpacity(0.8),
        height: double.infinity,
        width: double.infinity,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              AppLoadingWidget(),
              SizedBox(
                height: AppSize.margin16,
              ),
              AppText.get(AppTextScale.overline, message,
                  params: AppTextParams(color: AppColors.text85)),
            ],
          ),
        ),
      ),
    );
  }
}
