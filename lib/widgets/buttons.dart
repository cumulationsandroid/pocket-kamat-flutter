import 'package:flutter/material.dart';

import '../common/colors.dart';
import '../common/size.dart';
import 'icon.dart';
import 'texts.dart';

/// Types of action button
enum ActionButtonType { primary, secondary, text }

/// Sizes of action button
enum ActionButtonSize { large, medium, small }

/// Provides an enabled action button used in the app.
class EnabledActionButton extends StatelessWidget {
  /// Text corresponding to the button.
  final String text;

  /// Callback on clicking the button.
  final Function onClick;

  /// Type of the action button.
  final ActionButtonType type;

  /// Size of the action button.
  final ActionButtonSize size;

  /// Action button icon.
  final String icon;

  /// Text color of the action button.
  final Color textColor;

  /// Focus node for action button.
  final FocusNode focusNode;

  EnabledActionButton({
    @required this.type,
    @required this.text,
    @required this.size,
    @required this.onClick,
    Key key,
    this.icon,
    this.textColor,
    this.focusNode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case ActionButtonType.primary:
        {
          switch (size) {
            case ActionButtonSize.large:
              {
                return Container(
                  height: AppSize.largeButtonHeight,
                  width: AppSize.largeButtonWidth,
                  child: FlatButton(
                    onPressed: onClick,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: Colors.white,
                      ),
                    ),
                    color: AppColors.primary100,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            case ActionButtonSize.medium:
              {
                return Container(
                  height: AppSize.mediumButtonHeight,
                  width: AppSize.mediumButtonWidth,
                  child: FlatButton(
                    autofocus: true,
                    focusNode: focusNode,
                    onPressed: onClick,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: Colors.white,
                      ),
                    ),
                    color: AppColors.primary100,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            case ActionButtonSize.small:
              {
                return Container(
                  height: AppSize.smallButtonHeight,
                  // width: AppSize.smallButtonWidth,
                  child: FlatButton(
                    onPressed: onClick,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: Colors.white,
                      ),
                    ),
                    color: AppColors.primary100,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            default:
              return Container();
          }

          break;
        }
      case ActionButtonType.secondary:
        {
          switch (size) {
            case ActionButtonSize.large:
              {
                return Container(
                  height: AppSize.largeButtonHeight,
                  width: AppSize.largeButtonWidth,
                  child: OutlineButton(
                    onPressed: onClick,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: AppColors.primary100,
                      ),
                    ),
                    color: AppColors.primary100,
                    highlightedBorderColor: AppColors.primary100,
                    borderSide: BorderSide(
                        color: AppColors.primary100, width: AppSize.stroke2),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            case ActionButtonSize.medium:
              {
                return Container(
                  height: AppSize.mediumButtonHeight,
                  width: AppSize.mediumButtonWidth,
                  child: OutlineButton(
                    onPressed: onClick,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: AppColors.primary100,
                      ),
                    ),
                    color: AppColors.primary100,
                    highlightedBorderColor: AppColors.primary100,
                    borderSide: BorderSide(
                        color: AppColors.primary100, width: AppSize.stroke2),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            case ActionButtonSize.small:
              {
                return Container(
                  height: AppSize.smallButtonHeight,
                  // width: AppSize.smallButtonWidth,
                  child: OutlineButton(
                    onPressed: onClick,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: AppColors.primary100,
                      ),
                    ),
                    color: AppColors.primary100,
                    highlightedBorderColor: AppColors.primary100,
                    borderSide: BorderSide(
                        color: AppColors.primary100, width: AppSize.stroke1),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            default:
              return Container();
          }

          break;
        }
      case ActionButtonType.text:
        {
          switch (size) {
            case ActionButtonSize.large:
              {
                return Container(
                  height: AppSize.largeButtonHeight,
                  width: AppSize.largeButtonWidth,
                  child: InkWell(
                    onTap: onClick,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AppText.get(
                          AppTextScale.headline,
                          text,
                          params: AppTextParams(
                            color: textColor ?? AppColors.primary100,
                          ),
                        ),
                        (icon != null) ? AppIcon(icon) : Container(),
                      ],
                    ),
                    borderRadius: BorderRadius.circular(AppSize.radius4),
                  ),
                );
              }
            case ActionButtonSize.medium:
              {
                return Container(
                  height: AppSize.mediumButtonHeight,
                  width: AppSize.mediumButtonWidth,
                  child: InkWell(
                    onTap: onClick,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AppText.get(
                          AppTextScale.headline,
                          text,
                          params: AppTextParams(
                            color: textColor ?? AppColors.primary100,
                          ),
                        ),
                        (icon != null) ? AppIcon(icon) : Container(),
                      ],
                    ),
                    borderRadius: BorderRadius.circular(AppSize.radius4),
                  ),
                );
              }
            case ActionButtonSize.small:
              {
                return Container(
                  height: AppSize.smallButtonHeight,
                  // width: AppSize.smallButtonWidth,
                  child: InkWell(
                    onTap: onClick,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        AppText.get(
                          AppTextScale.headline,
                          text,
                          params: AppTextParams(
                            color: textColor ?? AppColors.primary100,
                          ),
                        ),
                        (icon != null) ? AppIcon(icon) : Container(),
                      ],
                    ),
                    borderRadius: BorderRadius.circular(AppSize.radius4),
                  ),
                );
              }
            default:
              return Container();
          }

          break;
        }
      default:
        return Container();
    }
  }
}

/// Provides an disabled action button used in the app.
class DisabledActionButton extends StatelessWidget {
  /// Text corresponding to the button.
  final String text;

  /// Type of the disabled action button.
  final ActionButtonType type;

  /// Size of the disabled action button.
  final ActionButtonSize size;

  /// Disabled action button icon.
  final String icon;

  DisabledActionButton(
    this.text, {
    @required this.type,
    @required this.size,
    this.icon,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case ActionButtonType.primary:
        {
          switch (size) {
            case ActionButtonSize.large:
              {
                return Container(
                  height: AppSize.largeButtonHeight,
                  width: AppSize.largeButtonWidth,
                  child: FlatButton(
                    onPressed: null,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: Colors.white,
                      ),
                    ),
                    color: AppColors.primary100,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            case ActionButtonSize.medium:
              {
                return Container(
                  height: AppSize.mediumButtonHeight,
                  width: AppSize.mediumButtonWidth,
                  child: FlatButton(
                    onPressed: null,
                    disabledColor: AppColors.inputFieldOutline,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: AppColors.text50,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            case ActionButtonSize.small:
              {
                return Container(
                  height: AppSize.smallButtonHeight,
                  // width: AppSize.smallButtonWidth,
                  child: FlatButton(
                    onPressed: null,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: AppColors.text50,
                      ),
                    ),
                    disabledColor: AppColors.inputFieldOutline,
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            default:
              return Container();
          }

          break;
        }
      case ActionButtonType.secondary:
        {
          switch (size) {
            case ActionButtonSize.large:
              {
                return Container(
                  height: AppSize.largeButtonHeight,
                  width: AppSize.largeButtonWidth,
                  child: OutlineButton(
                    onPressed: null,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: AppColors.text50,
                      ),
                    ),
                    color: AppColors.inputFieldOutline,
                    highlightedBorderColor: AppColors.inputFieldOutline,
                    borderSide: BorderSide(
                        color: AppColors.inputFieldOutline,
                        width: AppSize.stroke2),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            case ActionButtonSize.medium:
              {
                return Container(
                  height: AppSize.mediumButtonHeight,
                  width: AppSize.mediumButtonWidth,
                  child: OutlineButton(
                    onPressed: null,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: AppColors.text50,
                      ),
                    ),
                    color: AppColors.primary100,
                    highlightedBorderColor: AppColors.primary100,
                    borderSide: BorderSide(
                        color: AppColors.primary100, width: AppSize.stroke2),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            case ActionButtonSize.small:
              {
                return Container(
                  height: AppSize.smallButtonHeight,
                  // width: AppSize.smallButtonWidth,
                  child: OutlineButton(
                    onPressed: null,
                    child: AppText.get(
                      AppTextScale.headline,
                      text,
                      params: AppTextParams(
                        color: AppColors.text50,
                      ),
                    ),
                    color: AppColors.primary100,
                    highlightedBorderColor: AppColors.primary100,
                    borderSide: BorderSide(
                        color: AppColors.primary100, width: AppSize.stroke1),
                    shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(AppSize.radius4),
                    ),
                  ),
                );
              }
            default:
              return Container();
          }

          break;
        }
      case ActionButtonType.text:
        {
          switch (size) {
            case ActionButtonSize.large:
              {
                return Container(
                  height: AppSize.largeButtonHeight,
                  width: AppSize.largeButtonWidth,
                  child: InkWell(
                    onTap: null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AppText.get(
                          AppTextScale.headline,
                          text,
                          params: AppTextParams(
                            color: AppColors.text50,
                          ),
                        ),
                        (icon != null) ? AppIcon(icon) : Container(),
                      ],
                    ),
                    borderRadius: BorderRadius.circular(AppSize.radius4),
                  ),
                );
              }
            case ActionButtonSize.medium:
              {
                return Container(
                  height: AppSize.mediumButtonHeight,
                  width: AppSize.mediumButtonWidth,
                  child: InkWell(
                    onTap: null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AppText.get(
                          AppTextScale.headline,
                          text,
                          params: AppTextParams(
                            color: AppColors.text50,
                          ),
                        ),
                        (icon != null) ? AppIcon(icon) : Container(),
                      ],
                    ),
                    borderRadius: BorderRadius.circular(AppSize.radius4),
                  ),
                );
              }
            case ActionButtonSize.small:
              {
                return Container(
                  height: AppSize.smallButtonHeight,
                  // width: AppSize.smallButtonWidth,
                  child: InkWell(
                    onTap: null,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        AppText.get(
                          AppTextScale.headline,
                          text,
                          params: AppTextParams(
                            color: AppColors.text50,
                          ),
                        ),
                        (icon != null) ? AppIcon(icon) : Container(),
                      ],
                    ),
                    borderRadius: BorderRadius.circular(AppSize.radius4),
                  ),
                );
              }
            default:
              return Container();
          }

          break;
        }
      default:
        return Container();
    }
  }
}
