import 'package:flutter/material.dart';

import '../common/size.dart';

/// Provides a widget loading the icons from the assets, wrapping them in the default icon shape
/// specified in the designs.
class AppIcon extends StatelessWidget {
  /// Specifies the path from where the icon is to be loaded.
  final String _icon;

  /// Specifies the key for this widget.
  final String valueKey;

  /// The icon size can be mentioned in this field.
  final double iconSize;

  /// Icon color can be specified here.
  final Color iconColor;

  const AppIcon(this._icon, {this.valueKey, this.iconSize, this.iconColor});

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      "assets/icons/" + _icon,
      height: (iconSize == null) ? AppSize.icon24 : iconSize,
      width: (iconSize == null) ? AppSize.icon24 : iconSize,
      key: ValueKey(valueKey) ?? null,
      color: iconColor ?? null,
    );
  }
}
