import 'package:flutter/material.dart';

import '../common/colors.dart';
import '../common/size.dart';
import '../common/styles.dart';
import 'password_field.dart';
import 'texts.dart';

/// Provides the common text input field used in the app.
class AppInputField {
  /// Provides an instance of [PasswordInputField] by passing all the field that
  /// [PasswordInputField] requires.
  static Widget password({
    Key key,
    @required TextEditingController controller,
    bool passwordVisibilityEnabled = true,
    String hint,
    String title,
    String error,
    bool autoFocus = false,
    int maxLength,
    Function validator,
    Function onChange,
    Function onEditingComplete,
    FocusNode focusNode,
  }) {
    return PasswordInputField(
      key: key,
      controller: controller,
      title: title,
      hint: hint,
      error: error,
      autoFocus: autoFocus,
      passwordVisibilityEnabled: passwordVisibilityEnabled,
      maxLength: maxLength,
      onChange: onChange,
      validator: validator,
      onEditingComplete: onEditingComplete,
      focusNode: focusNode,
    );
  }

  /// Returns a text field used in the app.
  ///
  /// Text field controller can be specified in [controller] field.
  /// Optional title can be specified in [title] field.
  /// Optional hint can be specified in [hint] field.
  /// The error message that is to be displayed can be specified in [error] field.
  /// Auto focuses the text field if the [autoFocus] field is set to true.
  /// [validator] callback that is called whenever the text field is validated.
  /// [onEditingComplete] that is called whenever the text field editing is complete.
  /// [keyboardType] specifies a default keyboard that is to shown when the user taps on this text field.
  /// The form key can be specified in [formKey] field.
  /// The focus node for this widget can be specified in [focusNode] field.
  static Widget text({
    Key key,
    @required TextEditingController controller,
    String title,
    String hint,
    String error,
    bool autoFocus = false,
    int maxLength,
    bool autoValidate,
    Function validator,
    Function onChange,
    Function onFieldSubmit,
    Function onEditingComplete,
    TextInputType keyboardType,
    TextInputAction textInputActions,
    Key formKey,
    FocusNode focusNode,
  }) {
    return Container(
      key: key,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          (title == null)
              ? Container()
              : AppText.get(AppTextScale.headline, title),
          SizedBox(
            height: AppSize.margin4,
          ),
          TextFormField(
            focusNode: focusNode,
            autofocus: autoFocus,
            controller: controller,
            autovalidate: (autoValidate == null) ? true : autoValidate,
            textInputAction: (textInputActions == null)
                ? TextInputAction.done
                : textInputActions,
            validator: (value) {
              if (validator != null) {
                return validator(value);
              }
              return null;
            },
            onChanged: (value) {
              // Perform tasks

              if (onChange != null) {
                onChange(value);
              }
            },
            onFieldSubmitted: (value) {
              if (onFieldSubmit != null) {
                onFieldSubmit(value);
              }
            },
            style: AppTextStyles.get(
              AppTextScale.headline,
            ),
            // onSubmitted: onSubmitted,
            onEditingComplete: onEditingComplete,
            maxLines: 1,
            minLines: 1,
            maxLengthEnforced: true,
            maxLength: maxLength,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              errorBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: AppColors.error),
              ),
              errorText: error,
              errorStyle: AppTextStyles.get(AppTextScale.caption,
                  params: AppTextParams(color: AppColors.error)),
              hintMaxLines: 1,
              hintText: hint,
              hintStyle: AppTextStyles.get(AppTextScale.overline,
                  params: AppTextParams(color: AppColors.text50)),
              border: const OutlineInputBorder(
                borderSide: const BorderSide(
                    color: AppColors.inputFieldOutline, width: 1),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Returns a text field with the unit at the end used in the app.
  ///
  /// Text field controller can be specified in [controller] field.
  /// Optional title can be specified in [title] field.
  /// Optional hint can be specified in [hint] field.
  /// The error message that is to be displayed can be specified in [error] field.
  /// Auto focuses the text field if the [autoFocus] field is set to true.
  /// [validate] callback that is called whenever the text field is validated.
  /// [onEditingComplete] that is called whenever the text field editing is complete.
  /// [onChange] that is called whenever the value in the text field changes.
  /// [onSubmitted] that is called whenever the user submits by clicking on the done button.
  /// The focus node for this widget can be specified in [focusNode] field.
  /// The maximum text length can be specified in [maxLength] field.
  static Widget textWithUnit({
    Key key,
    @required TextEditingController controller,
    String title,
    @required String unit,
    String hint,
    String error,
    int maxLength,
    bool autoFocus = false,
    Function onSubmitted,
    Function onChange,
    Function validate,
    Function onEditingComplete,
  }) {
    return Container(
      key: key,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          (title == null)
              ? Container()
              : AppText.get(AppTextScale.headline, title),
          SizedBox(
            height: AppSize.margin4,
          ),
          TextFormField(
            controller: controller,
            onChanged: (value) {
              // Perform tasks

              if (onChange != null) {
                onChange(value);
              }
            },
            style: AppTextStyles.get(
              AppTextScale.headline,
            ),
            autofocus: autoFocus,
            validator: (val) {
              if (val != null) return validate(val);
              return null;
            },
//            onSubmitted: onSubmitted,
            onEditingComplete: onEditingComplete,
            maxLines: 1,

            autovalidate: false,
            minLines: 1,
            maxLengthEnforced: true,
            keyboardType: TextInputType.numberWithOptions(),
            maxLength: maxLength,
            decoration: InputDecoration(
              errorBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: AppColors.error),
              ),
              errorText: error,
              errorStyle: AppTextStyles.get(AppTextScale.caption,
                  params: AppTextParams(color: AppColors.error)),
              hintMaxLines: 1,
              hintText: hint,
              hintStyle: AppTextStyles.get(AppTextScale.overline,
                  params: AppTextParams(color: AppColors.text50)),
              border: const OutlineInputBorder(
                borderSide: const BorderSide(
                    color: AppColors.inputFieldOutline, width: 1),
              ),
              suffixText: unit,
              suffixStyle: AppTextStyles.get(
                AppTextScale.headline,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
