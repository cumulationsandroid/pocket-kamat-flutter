import 'package:flutter/material.dart';

import '../common/colors.dart';
import '../common/constants.dart';
import '../common/size.dart';
import '../common/styles.dart';
import 'texts.dart';

/// Provides the widget having the text field to enter the password,
/// where the text is optionally obscured.
class PasswordInputField extends StatefulWidget {
  /// Text field controller of this widget.
  final TextEditingController controller;

  /// Title of the password field.
  String title;

  /// Hint of the password field.
  String hint;

  /// Error message that is to be displayed in the password field.
  String error;

  /// The text field auto focuses if this flag is set to true.
  bool autoFocus;

  /// The password is visible if this flag is set to true.
  bool passwordVisibilityEnabled;

  /// Validator callback that is called whenever the text field is validated.
  Function validator;

  /// Callback that is called whenever the text field editing is complete.
  Function onEditingComplete;

  /// Callback that is called whenever the text field value is changed.
  Function onChange;

  /// Focus node of this text field.
  FocusNode focusNode;

  /// Maximum characters allowed in the text field.
  int maxLength = Constants.defaultMaxPasswordLength;

  PasswordInputField({
    Key key,
    @required this.controller,
    this.title,
    this.hint,
    this.autoFocus = false,
    this.error,
    this.passwordVisibilityEnabled = true,
    this.maxLength,
    this.validator,
    this.onChange,
    this.onEditingComplete,
    this.focusNode,
  }) : super(key: key);

  @override
  _PasswordInputFieldState createState() => _PasswordInputFieldState();
}

class _PasswordInputFieldState extends State<PasswordInputField> {
  bool isPasswordHidden = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          (widget.title == null)
              ? Container()
              : AppText.get(AppTextScale.headline, widget.title),
          SizedBox(
            height: AppSize.margin4,
          ),
          TextFormField(
            autofocus: widget.autoFocus,
            focusNode: widget.focusNode,
            controller: widget.controller,
            autovalidate: true,
            onChanged: (value) {
              // Perform tasks

              if (widget.onChange != null) {
                widget.onChange(value);
              }
            },
            validator: (value) {
              if (widget.validator != null) {
                return widget.validator(value);
              }
              return null;
            },
            onEditingComplete: widget.onEditingComplete,
            maxLines: 1,
            minLines: 1,
            maxLengthEnforced: true,
            maxLength: widget.maxLength,
            obscureText: (isPasswordHidden && widget.passwordVisibilityEnabled),
            style: AppTextStyles.get(AppTextScale.headline),
            decoration: InputDecoration(
              errorBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: AppColors.error),
              ),
              errorText: widget.error,
              errorStyle: AppTextStyles.get(AppTextScale.caption,
                  params: AppTextParams(color: AppColors.error)),
              hintMaxLines: 1,
              hintText: widget.hint,
              hintStyle: AppTextStyles.get(AppTextScale.overline,
                  params: AppTextParams(color: AppColors.text50)),
              border: const OutlineInputBorder(
                borderSide: const BorderSide(
                    color: AppColors.inputFieldOutline, width: 1),
              ),
              suffixIcon: (!widget.passwordVisibilityEnabled)
                  ? null
                  : (isPasswordHidden)
                      ? IconButton(
                          icon: Icon(
                            Icons.visibility,
                            color: Colors.grey,
                          ),
                          onPressed: _togglePasswordVisibility,
                        )
                      : IconButton(
                          icon: Icon(
                            Icons.visibility_off,
                            color: Colors.grey,
                          ),
                          onPressed: _togglePasswordVisibility,
                        ),
            ),
          ),
        ],
      ),
    );
  }

  /// Toggles the password visibility.
  _togglePasswordVisibility() {
    setState(() {
      isPasswordHidden = !isPasswordHidden;
    });
  }
}
