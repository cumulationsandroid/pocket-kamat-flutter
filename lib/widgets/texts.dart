import 'package:flutter/material.dart';

import '../common/colors.dart';
import '../common/styles.dart';

/// Text params that can be provided to [AppText] in order to customize the
/// text that is provided by the [AppText].
class AppTextParams {
  /// Color of the text.
  /// 
  /// Default value is [AppColors.text100].
  final Color color;

  /// Max lines of the text.
  /// 
  /// Default value is 1.
  final int maxLines;

  /// Text alignment of the text.
  /// 
  /// Default value is [TextAlign.start].
  final TextAlign textAlign;

  /// Font weight of the text
  /// 
  /// Default value is [FontWeight.normal].
  final FontWeight fontWeight;

  const AppTextParams({
    this.color = AppColors.text100,
    this.maxLines = 1,
    this.textAlign = TextAlign.start,
    this.fontWeight = FontWeight.normal,
  });
}

enum AppTextScale {
  overline,
  overline1,
  overline2,
  caption,
  caption1,
  headline,
  headline2,
  title1,
  title2,
  largeTitle,
  body,
  footer,
  footer2,
  test
}


/// Provides an [Text] widget that is used throughout the app.
class AppText {

  /// Returns the [Text] widget based on the parameters provided.
  /// 
  /// The scales corresponding to the scales in the designs has to be 
  /// mentioned in the [scale] field.
  /// The string text value has to be specified in [text] field.
  /// The widget's key can be specified in [key] field.
  /// The text can be customised by using the [params] field.
  /// 
  static get(AppTextScale scale, String text,
      {Key key, AppTextParams params = const AppTextParams()}) {
    return Text(
      text,
      key: key,
      style: AppTextStyles.get(scale, params: params),
      maxLines: params.maxLines,
      overflow: TextOverflow.ellipsis,
      textAlign: params.textAlign,
    );
  }
}
